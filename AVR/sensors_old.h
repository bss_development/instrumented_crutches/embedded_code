#ifndef _SENSORS_H_
#define _SENSORS_H_ 1

#include "i2c-uart.h"


/*
Sensor 1 D1
Sensor 2 D0
Sensor 3 C1
Sensor 4 F0
Sensor 5 F1
*/
/* NOTE: IF ANYTHING CHANGES HERE, ALSO CHANGE MAPPING IN I2C-UART.H */
#define SENS1_ACC_DRDY_vect PORTD_INT1_vect
#define SENS1_INTMASK       INT1MASK
#define SENS1_ACC_DRDY_BM   0x20
#define SENS1_MAG_DRDY_BM   0x10
#define SENS1_PORT          PORTD
#define SENS1_INTLVL_HI     PORT_INT1LVL_HI_gc;
#define SENS1_INTLVL_OFF    PORT_INT1LVL_OFF_gc;
#define SENS2_ACC_DRDY_vect PORTD_INT0_vect
#define SENS2_INTMASK       INT0MASK
#define SENS2_ACC_DRDY_BM   0x02
#define SENS2_MAG_DRDY_BM   0x01
#define SENS2_PORT          PORTD
#define SENS2_INTLVL_HI     PORT_INT0LVL_HI_gc;
#define SENS2_INTLVL_OFF    PORT_INT0LVL_OFF_gc;
#define SENS3_ACC_DRDY_vect PORTC_INT1_vect
#define SENS3_INTMASK       INT1MASK
#define SENS3_ACC_DRDY_BM   0x20
#define SENS3_MAG_DRDY_BM   0x10
#define SENS3_PORT          PORTC
#define SENS3_INTLVL_HI     PORT_INT1LVL_HI_gc;
#define SENS3_INTLVL_OFF    PORT_INT1LVL_OFF_gc;
#define SENS4_ACC_DRDY_vect PORTF_INT0_vect
#define SENS4_INTMASK       INT0MASK
#define SENS4_ACC_DRDY_BM   0x02
#define SENS4_MAG_DRDY_BM   0x01
#define SENS4_PORT          PORTF
#define SENS4_INTLVL_HI     PORT_INT0LVL_HI_gc;
#define SENS4_INTLVL_OFF    PORT_INT0LVL_OFF_gc;
#define SENS5_ACC_DRDY_vect PORTF_INT1_vect
#define SENS5_INTMASK       INT1MASK
#define SENS5_ACC_DRDY_BM   0x20
#define SENS5_MAG_DRDY_BM   0x10
#define SENS5_PORT          PORTF
#define SENS5_INTLVL_HI     PORT_INT1LVL_HI_gc;
#define SENS5_INTLVL_OFF    PORT_INT1LVL_OFF_gc;

//HMC5883L
#define HMC_READ       0x3D
#define HMC_WRITE      0x3C

#define HMC_CRA         0
#define HMC_CRB         1
#define HMC_MR          2
#define HMC_DXRA        3
#define HMC_DXRB        4
#define HMC_DZRA        5
#define HMC_DZRB        6
#define HMC_DYRA        7
#define HMC_DYRB        8
#define HMC_SR          9
#define HMC_IRA        10
#define HMC_IRB        11
#define HMC_IRC        12

//HMC5883L CRA
#define AVG_MASK    0b11<<5
#define AVG_8       0b11<<5
#define AVG_4       0b10<<5
#define AVG_2       0b01<<5
#define AVG_1       0b00<<5

#define DOR_MASK    0b111<<2
#define DOR_0_75HZ  0b000<<2
#define DOR_1_5HZ   0b001<<2
#define DOR_3HZ     0b010<<2
#define DOR_7_5HZ   0b011<<2
#define DOR_15HZ    0b100<<2
#define DOR_30HZ    0b101<<2
#define DOR_75HZ    0b110<<2
//#define DOR_NA 0b111<<2

#define MM_MASK     0b11<<0
#define MM_NORMAL   0b00
#define MM_POS      0b01
#define MM_NEG      0b10
//#define MM_RESERVED

// HMC5883L CRB
#define GN_MASK     0b111<<5
#define GN_0_88GA   0b000<<5
#define GN_1_3GA    0b001<<5
#define GN_1_9GA    0b010<<5
#define GN_2_5GA    0b011<<5
#define GN_4_0GA    0b100<<5
#define GN_4_7GA    0b101<<5
#define GN_5_6GA    0b110<<5
#define GN_8_1GA    0b111<<5

// HMC5883L MR
#define MD_MASK     0b11
#define MD_CONT     0b00
#define MD_SING     0b01
#define MD_IDLE0    0b10
#define MD_IDLE1    0b11

//DATA registers already defined

//HMC5883L SR
#define HMC_LOCK        1<<1
#define HMC_RDY         1


//BMA180

#define BMA_READ       0x81
#define BMA_WRITE      0x80
#define BMA_EEPROM_OS       0x20
#define BMA_CHIP_ID         0
#define BMA_VERSION         1
#define BMA_ACC_X_LSB       2
#define BMA_ACC_X_MSB       3
#define BMA_ACC_Y_LSB       4
#define BMA_ACC_Y_MSB       5
#define BMA_ACC_Z_LSB       6
#define BMA_ACC_Z_MSB       7
#define BMA_TEMP            8
#define BMA_STATUS_REG1     9
#define BMA_STATUS_REG2     10
#define BMA_STATUS_REG3     11
#define BMA_STATUS_REG4     12
#define BMA_CTRL_REG0       13
#define BMA_CTRL_REG1       14
#define BMA_CTRL_REG2       15
#define BMA_RESET           16
#define BMA_BW_TCS          32
#define BMA_CTRL_REG3       33
#define BMA_CTRL_REG4       34
#define BMA_HY              35
#define BMA_CD1             44
#define BMA_CD2             45
#define BMA_TCO_X           46
#define BMA_TCO_Y           47
#define BMA_TCO_Z           48
#define BMA_GAIN_T          49
#define BMA_GAIN_X          50
#define BMA_GAIN_Y          51
#define BMA_GAIN_Z          52
#define BMA_OFFSET_LSB1     53
#define BMA_OFFSET_LSB2     54
#define BMA_OFFSET_T        55
#define BMA_OFFSET_X        56
#define BMA_OFFSET_Y        57
#define BMA_OFFSET Z        58

//BMA180 data registers, LSB mask
#define BMA_NEW_DATA_MASK   0b1

//BMA180 ctrl_reg0
#define BMA_EE_W 		0x10

//BMA180 ctrl_reg3
#define BMA_NEW_DATA_INT    0b1<<1

//BMA180 bw_tcs
#define BW_MASK         0b1111<<4
#define BW_10           0b0000<<4
#define BW_20           0b0001<<4
#define BW_40           0b0010<<4
#define BW_75           0b0011<<4
#define BW_150          0b0100<<4
#define BW_300          0b0101<<4
#define BW_600          0b0110<<4
#define BW_1200         0b0111<<4
#define BW_HP           0b1000<<4
#define BW_BP           0b1001<<4

//BMA180 reset
#define BMA_RESET_VALUE 0xB6

//BMA180 ctrl_reg1
#define BMA_DIS_WAKE_UP 1<<0
#define BMA_SLEEP       1<<1
#define BMA_ST0         1<<2
#define BMA_ST1         1<<3
#define BMA_EE_W        1<<4
#define BMA_UPDATE_IMG  1<<5
#define BMA_RESET_INT   1<<6

//BMA180 offset lsb1
#define RANGE_MASK      0b111<<1
#define RANGE_1G        0b000<<1
#define RANGE_1G5       0b001<<1
#define RANGE_2G        0b010<<1
#define RANGE_3G        0b011<<1
#define RANGE_4G        0b100<<1
#define RANGE_8G        0b101<<1
#define RANGE_16G       0b110<<1
//#define RANGE_NA        0b111<<1 //Not supported
#define BMA_SMP_SKIP    1

//BMA180 tco_z
#define MODE_CONFIG_MASK    0b11
#define MODE_LN         0b00
#define MODE_ULN        0b01
#define MODE_LN_LP      0b10
#define MODE_LP         0b11

//BMA180 offset_t
#define READOUT_12BIT   1

#define I2C_START           'S'
#define I2C_STOP            'P'
#define I2C_READ_INT_REG    'R'
#define I2C_WRITE_INT_REG   'W'
#define I2C_READ_GPIO       'I'
#define I2C_WRITE_GPIO      'O'
#define I2C_POWERDOWN       'Z'

inline void SensorWriteRegValue(I2CPORT *,uint8_t, uint8_t, uint8_t);
void SensorSetupBMA180(I2CPORT * port);
void SensorSetupHMC(I2CPORT * port);
// Declaration of arrays, Definition is in sensors.c
extern uint8_t SensorHMCStartSingle[];
extern uint8_t SensorHMCGetData[];
extern uint8_t SensorBMAGetData[];
// End of declaration
void SensorEnableBMAPinInterrupts(void);
void SensorSetupBMAPinInterrupts(void);
#endif /* _SENSORS_H_ */

