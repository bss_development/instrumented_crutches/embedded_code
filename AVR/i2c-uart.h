#ifndef _I2C_UART_H_
    #define _I2C_UART_H_ 1

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

//UART MAPPING
/*
Sensor 1 D1
Sensor 2 D0
Sensor 3 C1
Sensor 4 F0
Sensor 5 F1
*/
/* NOTE: IF ANYTHING CHANGES HERE, ALSO CHANGE MAPPING IN SENSORS.H */
#define I2C_UART1 USARTD1
#define I2C_UART1_RXC_vect USARTD1_RXC_vect
#define I2C_UART1_TXC_vect USARTD1_TXC_vect
#define I2C_UART1_DRE_vect USARTD1_DRE_vect
#define I2C_UART2 USARTD0
#define I2C_UART2_RXC_vect USARTD0_RXC_vect
#define I2C_UART2_TXC_vect USARTD0_TXC_vect
#define I2C_UART2_DRE_vect USARTD0_DRE_vect
#define I2C_UART3 USARTC1
#define I2C_UART3_RXC_vect USARTC1_RXC_vect
#define I2C_UART3_TXC_vect USARTC1_TXC_vect
#define I2C_UART3_DRE_vect USARTC1_DRE_vect
#define I2C_UART4 USARTF0
#define I2C_UART4_RXC_vect USARTF0_RXC_vect
#define I2C_UART4_TXC_vect USARTF0_TXC_vect
#define I2C_UART4_DRE_vect USARTF0_DRE_vect
#define I2C_UART5 USARTF1
#define I2C_UART5_RXC_vect USARTF1_RXC_vect
#define I2C_UART5_TXC_vect USARTF1_TXC_vect
#define I2C_UART5_DRE_vect USARTF1_DRE_vect

#define I2C_BUFFER_LENGTH   70

//SC18IM700 Internal registers
#define BRG0        0
#define BRG1        1
#define PORTCONF1   2
#define PORTCONF2   3
#define IOSTATE     4
#define I2CADR      6
#define I2CCLKL     7
#define I2CCLKH     8
#define I2CTO       9
#define I2CSTAT     10

//LED
#define LED_RED_OFF_bm     1
#define LED_GREEN_OFF_bm   2

enum I2C_STATE {I2C_IDLE = 0, I2C_BUSY, I2C_DATAREADY, I2C_LASTBYTESENT};

typedef struct ch_i2c_port
{
    volatile uint8_t     counter;                    //counter to count to quelength. When counter == quelength, message_complete = 1
    volatile uint8_t     message_complete;           //tx or rx complete
    uint8_t              quelength;                  //number of bytes to be transferred
    uint8_t *            datapointer;                //pointer to (external) data array
    uint8_t              buffer[I2C_BUFFER_LENGTH];  //internal data array
    volatile enum I2C_STATE       state;                      //state to keep track of reception
} CH_I2CPORT;

typedef struct i2c_uart
{
    USART_t * uart;
    CH_I2CPORT rx;
    CH_I2CPORT tx;
    volatile uint8_t iostate;
}I2CPORT;

I2CPORT i2cport1, i2cport2, i2cport3, i2cport4, i2cport5;

void I2CUartInitialize(I2CPORT *, USART_t *);
void I2CStartMultiTransfer(uint8_t *);
inline void I2CSetBaudrateFast(I2CPORT *);
inline void I2CSetBaudrateSlow(I2CPORT *);
uint8_t I2CSendMessage(I2CPORT *, uint8_t *);
uint8_t I2CSendMessageByte(I2CPORT *);
void I2CWriteIOPort(I2CPORT *, uint8_t);
void I2CPrepareReceiver(I2CPORT *, uint8_t, uint8_t *);
uint8_t * I2CReadBytes(I2CPORT *, uint8_t , uint8_t, uint8_t);
uint8_t I2CcheckIfBusFree(I2CPORT *, uint8_t);
#endif //ifndef _I2C-UART_H_
