#include <util/delay.h>
#include "i2c-uart.h"
#include "sensors.h"

void I2CUSARTInitialize(USART_t *);
void I2CWriteIOConfig(I2CPORT *);
void I2CSetI2CRateFast(I2CPORT *);
inline void I2CPrepareMultiTransfer(I2CPORT *, uint8_t *);
uint8_t I2CwritebridgeUartSpeed[] = {6,I2C_WRITE_INT_REG , BRG0 , 2 , BRG1 , 0 , I2C_STOP };
uint8_t I2CwritebridgeI2CSpeed[] = {6,I2C_WRITE_INT_REG , I2CCLKL , 2 , I2CCLKH , 0 , I2C_STOP };
static uint8_t I2CReadI2CBytes[] = {8, I2C_START , BMA_WRITE , 1, 0x00 /*reg*/ , I2C_START, BMA_READ , 1 , I2C_STOP };
static uint8_t I2CControlConfig[] = {6,I2C_WRITE_INT_REG, PORTCONF1, 0xAA, PORTCONF2, 0xAA, I2C_STOP};    //All pins defined as outputs
static uint8_t I2CControlLed[] = {4,I2C_WRITE_INT_REG, IOSTATE, 0x00, I2C_STOP};                          //output register write
static uint8_t I2CReadBridgeI2CStatus[] = {3,I2C_READ_INT_REG,I2CSTAT,I2C_STOP};
static uint8_t I2CwritebridgeI2CTimeOut[] = { 4, I2C_WRITE_INT_REG, I2CTO, ((112<<1)|0x01), I2C_STOP}; //half second timeout | enable
static inline void I2CDreRoutine(I2CPORT *);
static inline void I2CRxcRoutine(I2CPORT *);

//INTERRUPTS I2C1
ISR(I2C_UART1_RXC_vect)
{
	I2CRxcRoutine(&i2cport1);
}

ISR(I2C_UART1_DRE_vect)
{
    I2CDreRoutine(&i2cport1);
}

//INTERRUPTS I2C2
ISR(I2C_UART2_RXC_vect)
{
	I2CRxcRoutine(&i2cport2);
}

ISR(I2C_UART2_DRE_vect)
{
    I2CDreRoutine(&i2cport2);
}

//INTERRUPTS I2C3
ISR(I2C_UART3_RXC_vect)
{
	I2CRxcRoutine(&i2cport3);
}

ISR(I2C_UART3_DRE_vect)
{
    I2CDreRoutine(&i2cport3);
}

//INTERRUPTS I2C4
ISR(I2C_UART4_RXC_vect)
{
	I2CRxcRoutine(&i2cport4);
}

ISR(I2C_UART4_DRE_vect)
{
    I2CDreRoutine(&i2cport4);
}

//INTERRUPTS I2C5
ISR(I2C_UART5_RXC_vect)
{
	I2CRxcRoutine(&i2cport5);

}

ISR(I2C_UART5_DRE_vect)
{
    I2CDreRoutine(&i2cport5);
}

static inline void I2CDreRoutine(I2CPORT * port)
{
    if(!(port->tx.state == I2C_LASTBYTESENT) )						// if previous byte sent was not the last in the array
    {
        if(!I2CSendMessageByte(port))           //returns 0 if last byte sent
        {
            port->tx.state = I2C_LASTBYTESENT;
        }
    }
    else
    {
        port->tx.message_complete = 1;
        port->uart->CTRLA &= ~USART_DREINTLVL_gm;        //clear intlvl bits
        port->uart->CTRLA |= USART_DREINTLVL_OFF_gc;     //disable DRE interrupt
        port->tx.state = I2C_IDLE;
    }
}

static inline void I2CRxcRoutine(I2CPORT * port)
{
    port->rx.datapointer[port->rx.counter] = port->uart->DATA;
    port->rx.counter++;
    if(port->rx.counter >= port->rx.quelength)
    {
        port->rx.message_complete = 1;
        port->uart->CTRLB &= ~USART_RXEN_bm;
        port->rx.state = I2C_DATAREADY;
    }
}

void I2CUartInitialize(I2CPORT * port, USART_t * uart)
{
    port->tx.counter=0;
    port->tx.message_complete = 1;
    port->tx.quelength = 0;
    port->rx.counter=0;
    port->rx.message_complete = 1;
    port->rx.quelength = 0;
    port->uart = uart;
    port->rx.state = I2C_IDLE;
    port->tx.state = I2C_IDLE;
    I2CUSARTInitialize(port->uart);
    //while(!I2CcheckIfBusFree(port,90));               //wait until bus is free, 5ms timeout
	I2CSetBaudrateFast(port);
    while(!I2CcheckIfBusFree(port,10));               //wait until bus is free
    I2CWriteIOConfig(port);
    while(!I2CcheckIfBusFree(port,10));               //wait until bus is free
    I2CSetI2CRateFast(port);
    while(!I2CcheckIfBusFree(port,10));               //wait until bus is free
    I2CSendMessage(port, I2CwritebridgeI2CTimeOut);
    while(!I2CcheckIfBusFree(port,10));               //wait until bus is free
}

/*returns 1 if bus is available after timeout*100us*/
uint8_t I2CcheckIfBusFree(I2CPORT * port, uint8_t timeout_100_us)
{
    uint8_t count;
    I2CPrepareReceiver(port, 1, port->rx.buffer);
    I2CSendMessage(port, I2CReadBridgeI2CStatus);
    for(count = 0; count < timeout_100_us ; count++)
    {
        _delay_us(100);
        if(port->rx.message_complete)       // if answer received
            count = timeout_100_us;         // exit for-loop
    }
	port->rx.state = I2C_IDLE;
    if(port->rx.message_complete)
    {
        if(port->rx.buffer[0] == 0xF0) //I2CSTAT = 0K
            return 1;
        else
            return 0;
    }
    else
        return 0;
}
uint8_t * I2CReadBytes(I2CPORT * port, uint8_t i2c_read_address, uint8_t start_reg, uint8_t no_of_bytes)
{
    I2CReadI2CBytes[2] = i2c_read_address-1;   // write address
    I2CReadI2CBytes[4] = start_reg;
    I2CReadI2CBytes[6] = i2c_read_address;
    I2CReadI2CBytes[7] = no_of_bytes;
    I2CPrepareReceiver(port, no_of_bytes, port->rx.buffer);       // zet receiver klaarvoor ontvangen  bytes
    I2CSendMessage(port, I2CReadI2CBytes);
    while(!(port->rx.message_complete));  // wacht tot bericht ontvangen
    return (port->rx.buffer);
}

inline void I2CSetBaudrateFast(I2CPORT * port)
{
    //operationeel: 363k6
    //bij 32MHz peripheral speed: BSCALE = -1, BSEL = 9
    //
    I2CwritebridgeUartSpeed[3] = 4;
    I2CwritebridgeUartSpeed[5] = 0;
    I2CSendMessage(port, I2CwritebridgeUartSpeed);
    while(port->tx.state != I2C_IDLE); //wait until message is sent completely and shift register is empty
	while(!(port->uart->STATUS & USART_DREIF_bm));
	_delay_ms(10);
    port->uart->BAUDCTRLA =  9;
    port->uart->BAUDCTRLB = ((-1 << 4) & 0xF0); //BSCALE = -1, MSB BSEL = 0)
    _delay_us(100);
}

inline void I2CSetBaudrateSlow(I2CPORT * port)
{
    //startup: 9k6
    //bij 32MHz peripheral speed: BSCALE = 0, BSEL = 207
    //
    I2CwritebridgeUartSpeed[3] = 240;
    I2CwritebridgeUartSpeed[5] = 2;
    I2CSendMessage(port, I2CwritebridgeUartSpeed);
    while(!port->tx.message_complete)
	//while(port->uart->STATUS & USART_TXCIF_bm)); //wait until message is sent completely and shift register is empty
    port->uart->BAUDCTRLA = 207;
    port->uart->BAUDCTRLB = 0;
    //_delay_ms(60);
}

void I2CSetI2CRateFast(I2CPORT * port)
{
    I2CwritebridgeI2CSpeed[3] = 5; // I2CCLKL
    I2CwritebridgeI2CSpeed[5] = 5; // I2CCLKH //I2CCLKH+ I2CCLKL = 10 -> 369kHz bitrate
    I2CSendMessage(port, I2CwritebridgeI2CSpeed);

}

void I2CUSARTInitialize(USART_t * uart)
{
    uart->CTRLA = USART_RXCINTLVL_HI_gc | USART_TXCINTLVL_OFF_gc | USART_DREINTLVL_OFF_gc; //DRE enabled in routine
    uart->CTRLB = 0;//USART_RXEN_bm | USART_TXEN_bm ; //CLK2X, MPCM, TXB8 = 0
    uart->CTRLC = USART_CMODE_ASYNCHRONOUS_gc | USART_PMODE_DISABLED_gc | USART_CHSIZE_8BIT_gc; //SBMODE = 0-> 1 stop bit
    //startup: 9k6
    //bij 32MHz peripheral speed: BSCALE = 0, BSEL = 207
    //operational:
    uart->BAUDCTRLA = 207; //LSB of BSEL
    uart->BAUDCTRLB = 0;   //SCALE = 0, MSB van BSEL = 0;
}

/* ************************************************************ */
/* I2C SendMessage                                              */
/* Arguments:    port        i2cport to be used                 */
/*              msgarray    pointer to byte array te be         */
/*                          transferred                         */
/*First byte of array to be transferred should be message length*/
/*retuns 1                  */
/* ************************************************************ */
uint8_t I2CSendMessage(I2CPORT * port, uint8_t * msgarray)
{
	while(port->tx.state != I2C_IDLE);
	port->tx.state = I2C_BUSY;
    port->tx.message_complete = 0;
    port->tx.quelength = msgarray[0];
    port->tx.datapointer = &msgarray[1];                  //point to second byte in array = first data byte
    port->tx.counter = 0;
    port->uart->CTRLB |= USART_TXEN_bm;
    port->uart->CTRLA &= ~USART_DREINTLVL_gm;        //clear intlvl bits
    if(!I2CSendMessageByte(port))                    // send first byte, if message is just 1 byte
        port->tx.state = I2C_LASTBYTESENT;           // indicate that next DRE interrupt is the last one
    port->uart->CTRLA |= USART_DREINTLVL_HI_gc;      //enable DRE interrupt
    return(1);
}

/* ************************************************************ */
/* I2C SendByte                                                 */
/* returns 0 when last byte of array is put in data buffer      */
/* ************************************************************ */
uint8_t I2CSendMessageByte(I2CPORT * port)
{
    port->uart->DATA = port->tx.datapointer[port->tx.counter];
    port->tx.counter++;
    if(port->tx.quelength <= port->tx.counter)
    {
        return 0;
    }
    else
        return 1;
}

void I2CWriteIOConfig(I2CPORT * port)
{
    I2CSendMessage(port, I2CControlConfig);
}

void I2CWriteIOPort(I2CPORT * port, uint8_t value)
{
    port->iostate       = value;          //synchronize local data with data in bridge
    I2CControlLed[3]    = value;
    I2CSendMessage(port, I2CControlLed);
}

void I2CStartMultiTransfer(uint8_t * msgarray)
{
    uint8_t firstdatabyte;
    firstdatabyte = msgarray[1];
    I2CPrepareMultiTransfer(&i2cport1, msgarray);
    I2CPrepareMultiTransfer(&i2cport2, msgarray);
    I2CPrepareMultiTransfer(&i2cport3, msgarray);
    I2CPrepareMultiTransfer(&i2cport4, msgarray);
    I2CPrepareMultiTransfer(&i2cport5, msgarray);
    i2cport1.uart->DATA = firstdatabyte;
	i2cport2.uart->DATA = firstdatabyte;
	i2cport3.uart->DATA = firstdatabyte;
	i2cport4.uart->DATA = firstdatabyte;
	i2cport5.uart->DATA = firstdatabyte;
    i2cport1.uart->CTRLA |= USART_DREINTLVL_HI_gc;      //enable DRE interrupt
    i2cport2.uart->CTRLA |= USART_DREINTLVL_HI_gc;      //enable DRE interrupt
    i2cport3.uart->CTRLA |= USART_DREINTLVL_HI_gc;      //enable DRE interrupt
    i2cport4.uart->CTRLA |= USART_DREINTLVL_HI_gc;      //enable DRE interrupt
    i2cport5.uart->CTRLA |= USART_DREINTLVL_HI_gc;      //enable DRE interrupt
}

inline void I2CPrepareMultiTransfer(I2CPORT * port, uint8_t * msgarray)
{
    port->tx.message_complete = 0;
    port->tx.quelength      = msgarray[0];
    port->tx.datapointer    = &msgarray[1];                  //point to second byte in array = first data byte
    port->tx.counter        = 1;                           //offset with 1 since SendMsgByte is not used for first byte
}

void I2CPrepareReceiver(I2CPORT * port, uint8_t quelength, uint8_t * target)
{
    //while(port->rx.state == I2C_BUSY);
    port->rx.state              = I2C_BUSY;
    port->rx.quelength          = quelength;
    port->rx.counter            = 0;
    port->rx.message_complete   = 0;
    port->rx.datapointer        = target;
    port->uart->CTRLB          |= USART_RXEN_bm;
}
