#include "uart.h"

uint8_t txbuffer[(UARTBUFFERSIZE+1)];
uint8_t rxbuffer[(UARTBUFFERSIZE+1)];
uint8_t txbufferindex;
uint8_t rxbufferreadindex;
volatile uint8_t rxbufferwriteindex;
volatile uint8_t rxquelength;

#define USBUART						  USARTC0
#define DMA_CH_TRIGSRC_USBUART_DRE_gc DMA_CH_TRIGSRC_USARTC0_DRE_gc
#define USBUART_RXC_vect			  USARTC0_RXC_vect

ISR(USBUART_RXC_vect)
{
	rxbuffer[rxbufferwriteindex] = USBUART.DATA;
	//increase pointer or wrap to zero
	if( rxbufferwriteindex < UARTBUFFERSIZE )
	{
		rxbufferwriteindex++;
	}
	else
	{
		rxbufferwriteindex = 0;
	}
	if(rxquelength <UARTBUFFERSIZE)		
		rxquelength++;
}


uint8_t USBUartRXQuelength(void)
{
	return (uint8_t) rxquelength;
}

uint8_t USBUartReadByteFromBuffer(void)
{
	uint8_t returnvalue;
	returnvalue = rxbuffer[rxbufferreadindex];
	if(rxbufferreadindex < UARTBUFFERSIZE)
	{
		rxbufferreadindex++;	
	}
	else
	{
		rxbufferreadindex = 0;
	}
	// Security measure, prevent negative quelenght values
	if(rxquelength)
		rxquelength--;
	return(returnvalue);
}

void USBUartSendArray(uint8_t * start_addr, uint8_t number_of_bytes)
{
	while(!(USBUART.STATUS & USART_DREIF_bm));
    DMA.CH1.TRFCNT    = number_of_bytes;
    DMA.CH1.SRCADDR0  = (( (uint32_t) start_addr) >> 0*8 ) & 0xFF;
	DMA.CH1.SRCADDR1  = (( (uint32_t) start_addr) >> 1*8 ) & 0xFF;
	DMA.CH1.SRCADDR2  = (( (uint32_t) start_addr) >> 2*8 ) & 0xFF;
	DMA.CH1.CTRLA    |= DMA_CH_ENABLE_bm;
}

void USBUartSendArray_P(const char * PROGMEM start_addr, uint8_t number_of_bytes)
{
	while(!(USBUART.STATUS & USART_DREIF_bm) || (DMA.STATUS & DMA_CH1BUSY_bm));
    DMA.CH1.TRFCNT    = number_of_bytes;
    DMA.CH1.SRCADDR0  = (( (uint32_t) start_addr) >> (0*8) ) & 0xFF;
	DMA.CH1.SRCADDR1  = (( (uint32_t) start_addr) >> (1*8) ) & 0xFF;
	DMA.CH1.SRCADDR2  = (( (uint32_t) start_addr) >> (2*8) ) & 0xFF;
	DMA.CH1.CTRLA    |= DMA_CH_ENABLE_bm;
}

// Initialize DMA for sending data
void USBUartDMASetup(void)
{
    DMA.CH1.CTRLA     = DMA_CH_SINGLE_bm | DMA_CH_BURSTLEN_1BYTE_gc;           // not enabled yet, no reset, no repeat, SINGLE transfer, one byte burst
    DMA.CH1.CTRLB     = DMA_CH_ERRINTLVL_OFF_gc | DMA_CH_TRNINTLVL_OFF_gc;     // no interrupts enabled
    DMA.CH1.ADDRCTRL  = DMA_CH_SRCRELOAD_NONE_gc | DMA_CH_SRCDIR_INC_gc | DMA_CH_DESTRELOAD_NONE_gc | DMA_CH_DESTDIR_FIXED_gc; // do not reload addresses, increment src pointer, keep dest pointer the same.
    DMA.CH1.TRIGSRC   = DMA_CH_TRIGSRC_USBUART_DRE_gc;
    DMA.CH1.REPCNT    = 0;
	DMA.CH1.DESTADDR0 = (( (uint32_t) &USBUART.DATA) >> 0*8 ) & 0xFF;
	DMA.CH1.DESTADDR1 = (( (uint32_t) &USBUART.DATA) >> 1*8 ) & 0xFF;
	DMA.CH1.DESTADDR2 = (( (uint32_t) &USBUART.DATA) >> 2*8 ) & 0xFF;
}

// Initialize UART
void USBUartInit(void)
{
	rxbufferwriteindex = rxbufferreadindex = 0;
	rxquelength = 0;
	USBUART.CTRLA = USART_RXCINTLVL_HI_gc | USART_TXCINTLVL_OFF_gc | USART_DREINTLVL_OFF_gc;                        //TX is started with DMA transfer.
    USBUART.CTRLB = USART_RXEN_bm | USART_TXEN_bm;
    USBUART.CTRLC = USART_CMODE_ASYNCHRONOUS_gc | USART_PMODE_DISABLED_gc | /*USART_SBMODE_bm |*/ USART_CHSIZE_8BIT_gc; // 8 data bits, 1 stop bits, no parity
   
    //USBUART.BAUDCTRLA =  31;   //baudrate: 230k4 at 32MHz peripheral clock (actual rate: 228571)
	USBUART.BAUDCTRLA =  65;   //baudrate: 115200 at 32MHz peripheral clock (actual rate: 115942)
    USBUART.BAUDCTRLB = ((-2 << 4) & 0xF0); //BSCALE = -2, MSB BSEL = 0)
}

// Puts one byte into data register, if empty. Exits with 0 when successful
uint8_t USBUartSendByte(uint8_t data)
{
	if(USBUART.STATUS & USART_DREIF_bm)
	{
		USBUART.DATA = data;
		return 0;	
	}
	else
		return 1;
}