#ifndef XBUS_H_
#define XBUS_H_ 1

#include <avr/io.h>
#include <avr/interrupt.h>

#define XBUS_BUFFER_LENGTH 60
#define AXIS_X 0
#define AXIS_Y 1
#define AXIS_Z 2

#define IMU_SYNC_MASK 0x01

#define ENABLE_IMU_SYNC_INTERRUPT PORTE.INTCTRL = PORT_INT0LVL_HI_gc

#define XBUS_UART_IMU                   USARTC0
#define XBUS_IMU_RXC_vect               USARTC0_RXC_vect
#define DMA_CH_TRIGSRC_USART_IMU_DRE_gc DMA_CH_TRIGSRC_USARTE0_DRE_gc
#define XBUS_UART_PC                    USARTC1
#define XBUS_PC_RXC_vect                USARTC1_RXC_vect
#define DMA_CH_TRIGSRC_USART_PC_DRE_gc  DMA_CH_TRIGSRC_USARTC1_DRE_gc

// Declaration of arrays. Definition is in xbus.c
extern uint8_t XbusImuMsgGoToMeasurement[];
extern uint8_t XbusImuMsgGoToConfig[];
extern uint8_t XbusImuMsgSetPeriod[];
extern uint8_t XbusImuMsgSetOutputMode[];
extern uint8_t XbusImuMsgSetSyncIn_SetSync[];
extern uint8_t XbusImuMsgSetSyncIn_SetSkip[];
extern uint8_t XbusImuMsgSetSyncIn_SetOffs[];
extern uint8_t XbusImuMsgSetSyncOut_SetSync[];
extern uint8_t XbusImuMsgSetSyncOut_SetSkip[];
extern uint8_t XbusImuMsgSetSyncOut_SetOffs[];
extern uint8_t XbusImuMsgSetSyncOut_SetWidth[];
// End of Declaration

enum XBUS_STATE {XBUS_IDLE = 0, XBUS_BID, XBUS_MID, XBUS_LEN, XBUS_DATA, XBUS_CS};

struct xbus_uart
{
    uint16_t    counter;
    uint8_t     checksum;
    uint8_t     checksum_ok;
    uint8_t     message_complete;
    uint8_t     quelength;
    uint8_t     buffer[XBUS_BUFFER_LENGTH];
    enum XBUS_STATE state;
};

struct xbus_struct
{
    USART_t * uart;
	volatile struct xbus_uart rx;
    volatile struct xbus_uart tx;
};

struct xbus_struct xbus_pc, xbus_imu;
//void InitUartRegisters(uint16_t, struct UART_REGISTERS *);

typedef struct xbus_message
{
    uint8_t     mid;
    uint8_t     bid;
    uint16_t    length;
    uint8_t     msg[(XBUS_BUFFER_LENGTH - 5)];
}xbus_msg;

struct imu_data
{
    uint16_t mag[3]; //6
    uint16_t acc[3]; //6
    uint16_t gyr[3]; //6
    uint16_t temp;   //2
    uint16_t sc;     //2 bytes: 22 bytes
};

struct xbus_data_frame
{
    uint8_t  preamble[4];  // 4
    uint16_t hmc_data1[3]; // 6
    uint16_t bma_data1[3]; // 6
    uint8_t  bma_temp1;    // 1
    uint16_t hmc_data2[3]; // 6
    uint16_t bma_data2[3]; // 6
    uint8_t  bma_temp2;    // 1
    uint16_t hmc_data3[3]; // 6
    uint16_t bma_data3[3]; // 6
    uint8_t  bma_temp3;    // 1
    uint16_t hmc_data4[3]; // 6
    uint16_t bma_data4[3]; // 6
    uint8_t  bma_temp4;    // 1
    uint16_t hmc_data5[3]; // 6
    uint16_t bma_data5[3]; // 6
    uint8_t  bma_temp5;    // 1
	struct   imu_data mtx; // 22
    uint8_t  hmc_status;   // 1
    uint8_t  imu_bma_status;// 1
    uint8_t  checksum;     // 1
}; //94 bytes

#define XBUS_DATA_FRAME_SIZE    94

void InitializeXbus(void);
void XbusPCSetup(void);
void XbusDMAPCSetup(void);
void XbusPCSendArray(uint8_t *, uint8_t);
void XbusIMUSetup(void);
void XbusDMAIMUSetup(void);
void XbusIMUSendArray(uint8_t *, uint8_t);
void XbusUartSetup(USART_t *);
void XbusSetupReceiver(struct xbus_struct *);
uint8_t XbusCreateChecksum(uint8_t *, uint8_t);
#endif
