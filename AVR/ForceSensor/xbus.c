#include "xbus.h"

static inline void XbusReceiveState(struct xbus_struct *, uint8_t);

uint8_t XbusImuMsgGoToMeasurement[] ={   0xFA,
                                                0xFF,
                                                0x10,
                                                0x00,
                                                0xF1};

uint8_t XbusImuMsgGoToConfig[] = { 0xFA,
                                          0xFF,
                                          0x30,
                                          0x00,
                                          0xD1};

uint8_t XbusImuMsgSetPeriod[] ={ 0xFA,
                                        0xFF,
                                        0x04, /* MID: SetPeriod */
                                        0x02,
                                        0x04, /* MSB time */
                                        0x80, /* LSB time */ /*time in 8.68us*/ /*0480 eq. 10ms*/
                                        0x77};

uint8_t XbusImuMsgSetOutputMode[] ={ 0xFA,
                                            0xFF,
                                            0xD0, /* MID: SetOutpuMode */
                                            0x02,
                                            0x40, /* MSB */
                                            0x00, /* LSB */ /*4000 eq. raw data mode*/
                                            0xEF};

uint8_t XbusImuMsgSetSyncIn_SetSync[] ={0xFA,
                                              0xFF,
                                              0xD6, /* MID: SetSyncIn */
                                              0x03,
                                              0x00, /* SyncInMode*/
                                              0x00, /* Reserved */
                                              0x01, /* Sample ADC, RisingEdge*/
                                              0x27};

uint8_t XbusImuMsgSetSyncIn_SetSkip[] ={0xFA,
                                              0xFF,
                                              0xD6, /* MID: SetSyncIn */
                                              0x03,
                                              0x01, /* SkipFactor*/
                                              0x00, /* MSB SkipFactor */
                                              0x00, /* LSB SkipFactor */
                                              0x27};

uint8_t XbusImuMsgSetSyncIn_SetOffs[] ={0xFA,
                                              0xFF,
                                              0xD6, /* MID: SetSyncIn */
                                              0x05,
                                              0x02, /* Offset*/
                                              0x00, /* Bit 31..24 */
                                              0x01, /* Bit 23..16 */
                                              0x06, /* Bit 15..8  */
                                              0xB9, /* Bit  7..0  */ /*minimum value: 264*33.9ns*/ /*E675 eq. 2ms*/
                                              0x64};

uint8_t XbusImuMsgSetSyncOut_SetSync[] ={0xFA,
                                              0xFF,
                                              0xD8, /* MID: SetSyncOut */
                                              0x03,
                                              0x00, /* SyncOutMode*/
                                              0x00, /* Reserved */
                                              0x12, /* Postitive Pulse, Pulse Mode*/
                                              0x14};

uint8_t XbusImuMsgSetSyncOut_SetSkip[] ={0xFA,
                                              0xFF,
                                              0xD8, /* MID: SetSyncOut */
                                              0x03,
                                              0x01, /* SkipFactor*/
                                              0x00, /* MSB SkipFactor */
                                              0x00, /* LSB SkipFactor */
                                              0x25};

uint8_t XbusImuMsgSetSyncOut_SetOffs[] ={0xFA,
                                              0xFF,
                                              0xD8, /* MID: SetSyncOut */
                                              0x05,
                                              0x02, /* Offset*/
                                              0x00, /* Bit 31..24 */
                                              0x00, /* Bit 23..16 */
                                              0xE6, /* Bit 15..8  */
                                              0x75, /* Bit  7..0  */ /*minimum value: 513*33.9ns*/ /*E675 eq. 2ms*/
                                              0xC7};

uint8_t XbusImuMsgSetSyncOut_SetWidth[] ={0xFA,
                                              0xFF,
                                              0xD8, /* MID: SetSyncOut */
                                              0x05,
                                              0x03, /* Width*/
                                              0x00, /* Bit 31..24 */
                                              0x00, /* Bit 23..16 */
                                              0xE6, /* Bit 15..8  */
                                              0x75, /* Bit  7..0  */ /*minimum value: 1700*33.9ns*/ /*E675 eq. 2ms*/
                                              0xC6};

//ISR(XBUS_IMU_RXC_vect)
//{
//    uint8_t rxdata;
//    rxdata = xbus_imu.uart->DATA; //read byte from UART
//    XbusReceiveState(&xbus_imu, rxdata);
//    if(xbus_imu.rx.message_complete && xbus_imu.rx.checksum_ok && (xbus_imu.rx.buffer[1] == 0xFF) && (xbus_imu.rx.buffer[2] != 50))          // If received complete message, checksum ok and BID = 0xFF
//	    XbusPCSendArray(xbus_imu.rx.buffer, (xbus_imu.rx.buffer[3] + 5));/*use LEN byte for message length*/                                 // Forward message to PC
//}

//ISR(XBUS_PC_RXC_vect)
//{
//   uint8_t rxdata;
//    rxdata = xbus_imu.uart->DATA; //read byte from UART
//    XbusReceiveState(&xbus_pc, rxdata);
//    if(xbus_pc.rx.message_complete && xbus_pc.rx.checksum_ok && (xbus_pc.rx.buffer[1] == 0xFF) )                // If received complete message, checksum ok and BID = 0xFF
//    	XbusIMUSendArray(xbus_pc.rx.buffer, (xbus_pc.rx.buffer[3] + 5)/*use LEN byte for message length*/ );    // Forward message to PC
//}

static inline void XbusReceiveState(struct xbus_struct * xbus, uint8_t rxdata)
{
    switch(xbus->rx.state)
    {
        case XBUS_IDLE:
        {
            if(rxdata == 0xFA)
            {
                xbus->rx.counter = 0;
                xbus->rx.buffer[0] = 0xFA;
                xbus->rx.message_complete = 0;
                xbus->rx.state = XBUS_BID;
            }
            break;
        }
        case XBUS_BID:
        {
            xbus->rx.counter = 1;
            xbus->rx.buffer[xbus->rx.counter] = rxdata;
            xbus->rx.checksum  = rxdata;
            xbus->rx.state = XBUS_MID;
            break;
        }
        case XBUS_MID:
        {
            xbus->rx.counter = 2;
            xbus->rx.buffer[xbus->rx.counter] = rxdata;
            xbus->rx.checksum  = xbus->rx.checksum + rxdata;
            xbus->rx.state = XBUS_LEN;
            break;
        }
        case XBUS_LEN:
        {
            if(rxdata > XBUS_BUFFER_LENGTH - 4)                                   // if message longer than buffer can contain
                xbus->rx.state  = XBUS_IDLE;                                           // EXLEN not supported!
            else
            {
                xbus->rx.counter = 3;
                xbus->rx.buffer[xbus->rx.counter] = rxdata;
                xbus->rx.checksum = xbus->rx.checksum + rxdata;
                if(rxdata == 0)                  // no data sent
                    xbus->rx.state = XBUS_CS;    // go to checksum
                else
                    xbus->rx.state = XBUS_DATA;
            }
            break;
        }
        case XBUS_DATA:
        {
            xbus->rx.checksum += rxdata;
            xbus->rx.counter++;
            xbus->rx.buffer[xbus->rx.counter] = rxdata;
            if(xbus->rx.counter == (xbus->rx.buffer[3] + 3) )     // if all data received (calculated from LEN
                xbus->rx.state = XBUS_CS;                         // go to checksum
            break;
        }
        case XBUS_CS:
        {
			volatile uint16_t cs_calc;
            xbus->rx.checksum_ok = 0;
            xbus->rx.message_complete = 1;
            xbus->rx.counter++;
            xbus->rx.buffer[xbus->rx.counter] = rxdata;
            //xbus->rx.checksum = 255 - xbus->rx.checksum;
			xbus->rx.state = XBUS_IDLE;
            cs_calc = xbus->rx.checksum + rxdata;
			if( (cs_calc & 0x00FF) == 0)
                xbus->rx.checksum_ok = 1;
            break;
        }
        default:
        {
            xbus->rx.state = XBUS_IDLE;
            break;
        }
    }
}
void InitializeXbus()
{
    XbusPCSetup();
    XbusIMUSetup();
}

void XbusPCSetup(void)
{
    xbus_pc.uart = &XBUS_UART_PC;
    xbus_pc.rx.state = XBUS_IDLE;
    xbus_pc.rx.message_complete = 1;
    XbusDMAPCSetup();
    XbusUartSetup(xbus_pc.uart);
}

void XbusSetupReceiver(struct xbus_struct * xbus)
{
    xbus->rx.message_complete = 0;
    xbus->rx.state = XBUS_IDLE;
    xbus->rx.counter = 0;
}
void XbusIMUSetup(void)
{
    xbus_imu.uart = &XBUS_UART_IMU;
    xbus_imu.rx.state = XBUS_IDLE;
    xbus_imu.rx.message_complete = 1;
    //PORTE.INTCTRL   = PORT_INT0LVL_OFF_gc;                       // Disable interrupt for PORTE, enable in main, after initalisation of sensors
    //PORTE.INT0MASK  = 0x01;                                      // IMU-Sync pin is source of interrupt
    //PORTE.PIN0CTRL  = PORT_OPC_TOTEM_gc | PORT_ISC_RISING_gc;    // interrupt on rising edge;
    XbusDMAIMUSetup();
    XbusUartSetup(xbus_imu.uart);

    XbusSetupReceiver(&xbus_imu);
    XbusIMUSendArray(XbusImuMsgGoToConfig, XbusImuMsgGoToConfig[3] + 5);
	while(DMA.CH0.CTRLA & DMA_CH_ENABLE_bm);
	while(!(xbus_imu.rx.message_complete));

	XbusSetupReceiver(&xbus_imu);
    XbusIMUSendArray(XbusImuMsgSetOutputMode, XbusImuMsgSetOutputMode[3] + 5);
	while(DMA.CH0.CTRLA & DMA_CH_ENABLE_bm);
	while(!(xbus_imu.rx.message_complete));

	XbusSetupReceiver(&xbus_imu);
	XbusIMUSendArray(XbusImuMsgSetPeriod, XbusImuMsgSetPeriod[3] + 5);
	while(DMA.CH0.CTRLA & DMA_CH_ENABLE_bm);
	while(!(xbus_imu.rx.message_complete));

	XbusSetupReceiver(&xbus_imu);
	XbusIMUSendArray(XbusImuMsgSetSyncIn_SetSync, XbusImuMsgSetSyncIn_SetSync[3] + 5 );
	while(DMA.CH0.CTRLA & DMA_CH_ENABLE_bm);
	while(!(xbus_imu.rx.message_complete));

	XbusSetupReceiver(&xbus_imu);
	XbusIMUSendArray(XbusImuMsgSetSyncIn_SetSkip, XbusImuMsgSetSyncIn_SetSkip[3] + 5 );
    while(DMA.CH0.CTRLA & DMA_CH_ENABLE_bm);
	while(!(xbus_imu.rx.message_complete));

	XbusSetupReceiver(&xbus_imu);
	XbusIMUSendArray(XbusImuMsgSetSyncIn_SetOffs, XbusImuMsgSetSyncIn_SetOffs[3] + 5 );
    while(DMA.CH0.CTRLA & DMA_CH_ENABLE_bm);
	while(!(xbus_imu.rx.message_complete));

	XbusSetupReceiver(&xbus_imu);
	XbusIMUSendArray(XbusImuMsgGoToMeasurement, XbusImuMsgGoToMeasurement[3] + 5);
   	while(DMA.CH0.CTRLA & DMA_CH_ENABLE_bm);
	while(!(xbus_imu.rx.message_complete));
}

void XbusUartSetup(USART_t * uart)
{
    uart->CTRLA = USART_RXCINTLVL_LO_gc | USART_TXCINTLVL_OFF_gc | USART_DREINTLVL_OFF_gc;                        //TX is started with DMA transfer.
    uart->CTRLB = USART_RXEN_bm | USART_TXEN_bm;
    uart->CTRLC = USART_CMODE_ASYNCHRONOUS_gc | USART_PMODE_DISABLED_gc | /*USART_SBMODE_bm |*/ USART_CHSIZE_8BIT_gc; // 8 data bits, 1 stop bits, no parity
    if(uart == &XBUS_UART_PC)
    {
        //baudrate: 230k4 at 32MHz peripheral clock
        uart->BAUDCTRLA =  31;
        uart->BAUDCTRLB = ((-2 << 4) & 0xF0); //BSCALE = -1, MSB BSEL = 0)
    }
    else
    {
        //baudrate: 115k2 at 32MHz peripheral clock
        uart->BAUDCTRLA =  33;
        uart->BAUDCTRLB = ((-1 << 4) & 0xF0); //BSCALE = -1, MSB BSEL = 0)
    }

}

void XbusDMAPCSetup(void)
{
    DMA.CH1.CTRLA     = DMA_CH_SINGLE_bm | DMA_CH_BURSTLEN_1BYTE_gc;           // not enabled yet, no reset, no repeat, SINGLE transfer, one byte burst
    DMA.CH1.CTRLB     = DMA_CH_ERRINTLVL_OFF_gc | DMA_CH_TRNINTLVL_OFF_gc;     // no interrupts enabled
    DMA.CH1.ADDRCTRL  = DMA_CH_SRCRELOAD_NONE_gc | DMA_CH_SRCDIR_INC_gc | DMA_CH_DESTRELOAD_NONE_gc | DMA_CH_DESTDIR_FIXED_gc; // do not reload addresses, increment src pointer, keep dest pointer the same.
    DMA.CH1.TRIGSRC   = DMA_CH_TRIGSRC_USART_PC_DRE_gc;
    DMA.CH1.REPCNT    = 0;
	DMA.CH1.DESTADDR0 = (( (uint32_t) &XBUS_UART_PC.DATA) >> 0*8 ) & 0xFF;
	DMA.CH1.DESTADDR1 = (( (uint32_t) &XBUS_UART_PC.DATA) >> 1*8 ) & 0xFF;
	DMA.CH1.DESTADDR2 = (( (uint32_t) &XBUS_UART_PC.DATA) >> 2*8 ) & 0xFF;
}

void XbusPCSendArray(uint8_t * start_addr, uint8_t number_of_bytes)
{
    DMA.CH1.TRFCNT    = number_of_bytes;
    DMA.CH1.SRCADDR0  = (( (uint32_t) start_addr) >> 0*8 ) & 0xFF;
	DMA.CH1.SRCADDR1  = (( (uint32_t) start_addr) >> 1*8 ) & 0xFF;
	DMA.CH1.SRCADDR2  = (( (uint32_t) start_addr) >> 2*8 ) & 0xFF;
	DMA.CH1.CTRLA    |= DMA_CH_ENABLE_bm;
}

void XbusDMAIMUSetup(void)
{
    DMA.CH0.CTRLA     = DMA_CH_SINGLE_bm | DMA_CH_BURSTLEN_1BYTE_gc;           // not enabled yet, no reset, no repeat, SINGLE transfer, one byte burst
    DMA.CH0.CTRLB     = DMA_CH_ERRINTLVL_OFF_gc | DMA_CH_TRNINTLVL_OFF_gc;     // no interrupts enabled
    DMA.CH0.ADDRCTRL  = DMA_CH_SRCRELOAD_NONE_gc | DMA_CH_SRCDIR_INC_gc | DMA_CH_DESTRELOAD_NONE_gc | DMA_CH_DESTDIR_FIXED_gc; // do not reload addresses, increment src pointer, keep dest pointer the same.
    DMA.CH0.TRIGSRC   = DMA_CH_TRIGSRC_USART_IMU_DRE_gc;
    DMA.CH0.REPCNT    = 0;
	DMA.CH0.DESTADDR0 = (( (uint32_t) &XBUS_UART_IMU.DATA) >> 0*8 ) & 0xFF;
	DMA.CH0.DESTADDR1 = (( (uint32_t) &XBUS_UART_IMU.DATA) >> 1*8 ) & 0xFF;
	DMA.CH0.DESTADDR2 = (( (uint32_t) &XBUS_UART_IMU.DATA) >> 2*8 ) & 0xFF;
}

void XbusIMUSendArray(uint8_t * start_addr, uint8_t number_of_bytes)
{
    DMA.CH0.TRFCNT    = number_of_bytes;
    DMA.CH0.SRCADDR0  = (( (uint32_t) start_addr) >> 0*8 ) & 0xFF;
	DMA.CH0.SRCADDR1  = (( (uint32_t) start_addr) >> 1*8 ) & 0xFF;
	DMA.CH0.SRCADDR2  = (( (uint32_t) start_addr) >> 2*8 ) & 0xFF;
	DMA.CH0.CTRLA    |= DMA_CH_ENABLE_bm;
}

uint8_t XbusCreateChecksum(uint8_t * array, uint8_t arraysize)
{
	uint8_t counter;
	uint16_t temp =0;
	uint8_t checksum;
	for(counter = 1; counter < (arraysize-1) ; counter++) //start at BID, end before checksum
	{
		temp += array[counter];
	}
	checksum = 0xFF - (uint8_t)(temp & 0x00FF);
	checksum++;
	return checksum;
}
