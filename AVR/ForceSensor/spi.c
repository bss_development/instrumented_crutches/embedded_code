#include "spi.h"

#define SPI_BUFFER_SIZE 30
#define STOP_EXT_ADC_CLOCK      TCD0.CTRLA = TC_CLKSEL_OFF_gc; TCD0.CTRLB &= ~(TC0_CCAEN_bm);
#define START_EXT_ADC_CLOCK     TCD0.CTRLA = TC_CLKSEL_DIV2_gc;TCD0.CTRLB |=   TC0_CCAEN_bm ;


typedef struct ADC_DAC_SPI_strct
{
    uint8_t     buffer[SPI_BUFFER_SIZE];
    uint8_t     transfer_ready;       //when true, read / write action was completed
    uint8_t     buffer_available ;    //when true, buffer may be written. Should be set after reading.
    uint8_t     quelength;            //number of bytes to transceive
    uint8_t     buffer_index;         //readpointer  to buffer
	uint8_t		data_handled;		  //true if data handled.
    enum spitargets  target;          //0 for ADC 1 for DAC
}ADC_DAC_SPI_t;

volatile ADC_DAC_SPI_t adc_dac_spi = {{},true,true,0,0,true};

//Interrupt handler for SPIF flag
ISR(SPI_ADC_DAC_vect)
{
    uint8_t rxbyte;
	//make CONV signal low
	ADC_1408_CONV_LOW;
    // read data register
    rxbyte = SPI_ADC_DAC->DATA;
    // store last received byte in buffer
    adc_dac_spi.buffer[adc_dac_spi.buffer_index] = rxbyte;
    // increment read buffer pointer to the new location. No more than SPI_BUFFER_SIZE bytes can be stored
    if(adc_dac_spi.buffer_index < SPI_BUFFER_SIZE)
        adc_dac_spi.buffer_index++;
    // decrement the number of bytes to read
    adc_dac_spi.quelength--;
    // if no more bytes have to be read / sent
    if(adc_dac_spi.quelength == 0)
    {
        // indicate that buffer is filled with complete message, needs to be read
        adc_dac_spi.transfer_ready = true;
		adc_dac_spi.data_handled = false;
        if(adc_dac_spi.target == ADC)
        {
            //Set SPI clock high
			ADC_1408_SPI->CTRL &= ~(SPI_ENABLE_bm);
            PORTD.OUTSET = 0x80;
            //Start external clock
            START_EXT_ADC_CLOCK;
        }
        if(adc_dac_spi.target == DAC)
        {
            // Chip select
            DAC_2636_SPI_PORT->OUTSET  = DAC_2636_N_CS;
        }
    }
    // if more [useful] bytes need to be sent
    else
    {
        // send useful data from txbuffer
        SPI_SendByte(SPI_ADC_DAC, adc_dac_spi.buffer[adc_dac_spi.buffer_index]);
    }
}

//SPI functions


// SPI Send Array
// Returns non-zero value for errors
uint8_t SPITransceiveArray(uint8_t * arraypointer, uint8_t number_of_bytes, uint8_t target)
{
    uint8_t index;
    // wait until buffer may be written
    if(adc_dac_spi.buffer_available == false)
        return SPI_ERROR_BUFFER;
    // no memory leaks....
    if(number_of_bytes < SPI_BUFFER_SIZE)
    {
		// protect memory from being overwritten
        adc_dac_spi.buffer_available = false;
		adc_dac_spi.transfer_ready   = false;
		// initialize conditions
		adc_dac_spi.target = target;
        adc_dac_spi.quelength = number_of_bytes;
        adc_dac_spi.buffer_index = 0;
		ADC_1408_SPI->CTRL |= SPI_ENABLE_bm;
        switch(target)
        {
            case ADC:
            {
                STOP_EXT_ADC_CLOCK;
                // SET ADC_CONV_CLK High
                ADC_1408_CONVCLOCK_LOW;
				SPI_ADC_1408_init();
                // Hold ADC value
                ADC_1408_CONV_HIGH;
				_delay_us(1);
				ADC_1408_CONVCLOCK_LOW;
				_delay_us(1);
				ADC_1408_CONVCLOCK_HIGH;				
                break;

            }
            case DAC:
            {
                SPI_DAC_2636_init();
                break;
            }
            default:
                break;
        }
        // copy array
        for ( index = 0 ; index < number_of_bytes ; index++ )
        {
            adc_dac_spi.buffer[index] =  arraypointer[index];
        }
        // Send first byte
        SPI_SendByte(SPI_ADC_DAC, adc_dac_spi.buffer[0]);
    }
    else
        return SPI_ERROR;
    return SPI_OK;
}

uint8_t * SPIbufferpointer(void)
{
	return (uint8_t *)adc_dac_spi.buffer;	
}

uint8_t SPITransferReady(void)
{
	return adc_dac_spi.transfer_ready;
}

uint8_t SPIDataHandled(void)
{
	return adc_dac_spi.data_handled;
}

void SPISetDataHandled(uint8_t value)
{
	adc_dac_spi.data_handled = value;
}

uint8_t SPIBufferAvailable(void)
{
	return adc_dac_spi.buffer_available;
}

enum spitargets SPITarget(void)
{
	return adc_dac_spi.target;
}

void SPIClearBuffer(void)
{
	if(adc_dac_spi.transfer_ready)
		adc_dac_spi.buffer_available = true;
}

inline void SPI_DAC_2636_init(void)
{
    DAC_2636_SPI->CTRL = ( SPI_CLK2X_bm | SPI_ENABLE_bm | SPI_MASTER_bm | SPI_MODE_3_gc | SPI_PRESCALER_DIV16_gc );
    DAC_2636_SPI->INTCTRL = SPI_INTLVL_HI_gc;
}

inline void SPI_ADC_1408_init(void)
{
    static uint8_t adc_clock_initialised = 0;
    if(!adc_clock_initialised)
    {
        InitADCClock();
        adc_clock_initialised = 1;
    }
    ADC_1408_SPI->CTRL = ( SPI_ENABLE_bm | SPI_MASTER_bm | SPI_MODE_2_gc | SPI_PRESCALER_DIV16_gc );
    ADC_1408_SPI->INTCTRL = SPI_INTLVL_HI_gc;
}

void InitADCClock(void)
{
    // Set clock source
    TCD0.CTRLA = TC_CLKSEL_DIV2_gc;
    // Set Compare Channel A , FRQ mode
    TCD0.CTRLB = (TC0_CCAEN_bm | TC_WGMODE_FRQ_gc);
    // No capture mode
    TCD0.CTRLC = 0;
    // No event actions
    TCD0.CTRLD = TC_EVACT_OFF_gc | TC_EVSEL_OFF_gc;;
    // No byte mode
    TCD0.CTRLE = 0;
    // No interrupt enables
    TCD0.INTCTRLA = 0;
    TCD0.INTCTRLB = 0;

    TCD0.CCA   = 2;
}

inline void SPI_SendByte(SPI_t * spi_port, uint8_t data)
{
    spi_port->DATA = data;
}

inline uint8_t SPI_RecvByte(SPI_t * spi_port)
{
    return spi_port->DATA;
}

uint8_t DAC_2636SelectRef(uint8_t internal)
{
	if(internal)
	{
		
	}
	else //external
	{
		
	}
	return 1;
}
//DAC functions LTC2336
uint8_t DAC_2636WriteSingleChannel(uint16_t value, uint8_t channel)
{
    uint8_t spivalues[3];
    //6-channel DAC. To use 'write all channels' (addr 15) use other functions.
    if(channel < 7 )
    {
        uint8_t spierror;
        spivalues[0] = LTC2636_CMD_ADDR(DAC_2636_WRITE_INPUT_AND_UPDATE_N,channel);
        //convert value for 12-bit representation. Last 4 bits are DNC for DAC
        spivalues[1] = (uint8_t)(value >> 4);
        spivalues[2] = (uint8_t)(value << 4);
        spierror = SPITransceiveArray(spivalues,3,DAC);
        if(spierror)
            return spierror;
		//Chip Select
        DAC_2636_SPI_PORT->OUTCLR  = DAC_2636_N_CS;
		
    }
    else
        return 1;
    // no errors
    return 0;
}

void DAC_2636_init(void)
{
    //SPI pins
    DAC_2636_SPI_PORT->DIRSET  = (DAC_2636_N_CS | SPI_MOSI | SPI_SCK); //MISO is made input automatically in Master mode
    DAC_2636_SPI_PORT->OUTSET  = DAC_2636_N_CS;
    //Configuration pins
    DAC_2636_CONF_PORT->DIRSET = (DAC_2636_N_LDAC | DAC_2636_N_CLR);
    DAC_2636_CONF_PORT->OUTSET = DAC_2636_N_LDAC;                    // Tie nLDAC High if not used
    DAC_2636_CONF_PORT->OUTSET = DAC_2636_N_CLR;                     // nCLR clears inputs to default state when driven low
}

//ADC functions LTC1408
void ADC_1408_init(void)
{
    //SPI pins
    ADC_1408_SPI_PORT->DIRSET  = (ADC_1408_CONV | ADC_1408_CONV_CLK);
    ADC_1408_SPI_PORT->OUTCLR  = ADC_1408_CONV;
    ADC_1408_SPI_PORT->OUTSET  = ADC_1408_CONV_CLK;
    //Configuration Pins
    ADC_1408_CONF_PORT->DIRSET = (ADC_1408_BIP | ADC_1408_SEL0 | ADC_1408_SEL1 | ADC_1408_SEL2);
    ADC_1408_CONF_PORT->OUTSET = ADC_1408_NR_INPUTS;
    ADC_1408_CONF_PORT->OUTSET = ADC_1408_BIP;
}
