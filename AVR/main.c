/*
 */
#define F_CPU 32000000UL //TODO: 32000000UL 
#include <avr/io.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <stdlib.h>

#include "xbus.h"
#include "spi.h"
#include "uart.h"
#include <stdint.h>

#define NRESET_USB_LOW      PORTC.OUTCLR = 0x02
#define NRESET_USB_HIGH     PORTC.OUTSET = 0x02

#define US_PERIOD        1000u //1000u=2ms  //TODO: 1000u

#define TURN_RED_LED_ON       PORTA.OUTCLR = 0x01
#define TURN_RED_LED_OFF      PORTA.OUTSET = 0x01
#define TURN_GREEN_LED_ON     PORTA.OUTCLR = 0x02
#define TURN_GREEN_LED_OFF    PORTA.OUTSET = 0x02

#define SYNC_OUT_LOW		      PORTC.OUTCLR = PIN5_bm
#define SYNC_OUT_HIGH		      PORTC.OUTSET = PIN5_bm

#define UINT12_MAX 4096

uint8_t prompt[4] = {0x0A,0x0D,0x3A,0x3E}; //newline, carriage return, :>


void	InitPorts(void);
void	SetClock(void);
void	SetTimer(void);
void	TerminalHandler(uint8_t);
uint8_t TerminalCommandsNoArgument(uint8_t *);
uint8_t TerminalCommandsWithArgument(uint8_t *, uint8_t, uint8_t *);
void	TerminalShowInfo(void);
void	EnableDMA(void);
void	CopyArray(uint8_t */*src*/, uint8_t */*dest*/, uint8_t /*no_of_bytes*/, uint8_t/*mode*/);
void ConvertADCResults(uint8_t *, int16_t *);
void itoa_adc(int16_t , uint8_t * , uint8_t, uint8_t);

enum COPY_MODE {CLONE = 0, COPY_BMA, COPY_HMC};     //used in array copy
enum SENSOR_STATE {STATE_HMC=0, STATE_BMA_INIT, STATE_BMA, STATE_IMU, STATE_IDLE};
volatile uint8_t aquire_sensor;
volatile uint8_t badinterrupt;
volatile uint8_t * adc_data_pointer;
struct xbus_data_frame xbus_df[2];
struct xbus_data_frame * xbus_prepare;
struct xbus_data_frame * xbus_send;

//DAC offsets, are written to the dac after initialization, and can be overwritten via the "d#" command.
uint16_t DAC_OFFSETS[6]={0x3b4, 0x339, 0x255, 0x244, 0x3a2, 0x19a};
uint8_t hexOffset_A[6] = "0x3b4 ";
uint8_t hexOffset_B[6] = "0x339 ";
uint8_t hexOffset_C[6] = "0x255 ";
uint8_t hexOffset_D[6] = "0x244 ";
uint8_t hexOffset_E[6] = "0x3a2 ";
uint8_t hexOffset_F[6] = "0x19a ";
//Neutral: {0x7FF, 0x7FF, 0x7FF, 0x7FF, 0x7FF, 0x7FF}
//Force Amp A: [948, 825, 597, 580, 930, 410] --> results in Voltage readings of -3000 unloaded		{0x3B4, 0x339, 0x255, 0x244, 0x3A2, 0x19A}
//Force Amp B: [0, 0, 197, 0, 0, 0] --> results in Voltage readings of -2000 unloaded		{0x0, 0x0, 0x0C5, 0x0, 0x0, 0x0}
	
int16_t adc_results[6];
uint8_t USBUartContinuousData;

const uint8_t TerminalInfo[] =	"\n\r*** VERSION 99.99 ***"
							"\n\r - a<0-5>: display ADC value"
							"\n\r - d<0-5>: display DAC value"
							"\n\r - d<0-5> <hex value>: set DAC value"
							"\n\r - s<s|m>: sync per sample | measurement"
							"\n\r - ??    : Show info \n:>"
							"\n\r - c<t|f>: Continuous data true | false";

//EEMEM uint16_t DAC_OFFSETS[6] = {0x0111,0x0111,0x0222,0x0222,0x0abc,0x0FFF,};

ISR(__vector_default) //catch unhandled interrupts
{
	badinterrupt = 1;
}


ISR(PORTE_INT0_vect) //pin interrupt PORTE, IMU Sync
{
    asm("nop");
}

ISR(TCC0_CCA_vect) // TODO   SYNC_IN_ACTIVE
{
    //ADC_1408_CONV_HIGH;
    SPITransceiveArray((uint8_t *)adc_data_pointer,12,ADC);
}

int main(void)
{
	//uint16_t dactest = 0xff;
	uint8_t adc_temp_results[12];

	adc_data_pointer = (uint8_t *)adc_temp_results;
	InitPorts();
	TURN_GREEN_LED_OFF;
	TURN_RED_LED_OFF;
	SYNC_OUT_LOW;
	//NRESET_USB_LOW;
	//switch to external crystal, set PLL
	SetClock();
	USBUartInit();
	USBUartDMASetup();
	//_delay_ms(700);
	//enable write to WDT register
	CCP       = CCP_IOREG_gc;
	//disable watchdog
	WDT.CTRL &= ~(WDT_ENABLE_bm);
	NRESET_USB_HIGH;
	//_delay_ms(5);			//startup I2C
	PMIC.CTRL |= PMIC_HILVLEN_bm | PMIC_LOLVLEN_bm ;
	DAC_2636_init();
	ADC_1408_init();
	sei();
	_delay_ms(10);
	SetTimer();
	EnableDMA();
	TURN_GREEN_LED_ON;
	USBUartSendArray(prompt, 4);
	
	//write hard coded offsets to the DAC":
	
	_delay_ms(5);
	uint32_t value;
	value = strtoul(hexOffset_A, NULL,16);
	DAC_2636WriteSingleChannel((uint16_t)value,DAC_2636_DAC_A);
	_delay_ms(5);						 
	value = strtoul(hexOffset_B, NULL,16);
	DAC_2636WriteSingleChannel((uint16_t)value,DAC_2636_DAC_B);
	_delay_ms(5);		
	value = strtoul(hexOffset_C, NULL,16);				 
	DAC_2636WriteSingleChannel((uint16_t)value,DAC_2636_DAC_C);
	_delay_ms(5);	
	value = strtoul(hexOffset_D, NULL,16);					 
	DAC_2636WriteSingleChannel((uint16_t)value,DAC_2636_DAC_D);
	_delay_ms(5);		
	value = strtoul(hexOffset_E, NULL,16);				 
	DAC_2636WriteSingleChannel((uint16_t)value,DAC_2636_DAC_E);
	_delay_ms(5);		
	value = strtoul(hexOffset_F, NULL,16);				 
	DAC_2636WriteSingleChannel((uint16_t)value,DAC_2636_DAC_F);
	_delay_ms(5);
	
	
	USBUartContinuousData = 0;
	for(;;)
	{
		// if bytes in receive buffer
		if(USBUartRXQuelength() > 0)
		{
			TerminalHandler(USBUartReadByteFromBuffer());
		}
		if(SPITransferReady() && (SPITarget() == ADC) && (SPIDataHandled() == false))
		{
			ConvertADCResults(SPIbufferpointer(),adc_results);
			SPIClearBuffer();
			SPISetDataHandled(true);
			if(USBUartContinuousData)   //start sampling upon USB command. To also enable sampling upon trigger in, add:|| SYNC_IN_ACTIVE
			{
				TURN_RED_LED_ON;
				SYNC_OUT_HIGH;
				uint8_t response[35];
				response[0] = 0x0D;
				itoa_adc(adc_results[0], &(response[1]), 16, 4);
				response[5] = ' ';
				itoa_adc(adc_results[1], &(response[6]), 16, 4);
				response[10] = ' ';
				itoa_adc(adc_results[2], &(response[11]), 16, 4);
				response[15] = ' ';
				itoa_adc(adc_results[3], &(response[16]), 16, 4);
				response[20] = ' ';
				itoa_adc(adc_results[4], &(response[21]), 16, 4);
				response[25] = ' ';
				itoa_adc(adc_results[5], &(response[26]), 16, 4);
				response[30] = 0x0A;
				USBUartSendArray(response,31);
			}
			else
			{
				TURN_RED_LED_OFF;
				SYNC_OUT_LOW;
			}
		}
		else
		SPIClearBuffer();
		if(badinterrupt)
		{
			TURN_RED_LED_OFF;
			SYNC_OUT_LOW;
		}
	}
}


void EnableDMA(void)
{
    DMA.CTRL = DMA_ENABLE_bm | DMA_DBUFMODE_DISABLED_gc | DMA_PRIMODE_CH0123_gc;
}

void TerminalShowInfo(void)
{
	uint8_t stringlength = 0;
	while(TerminalInfo[stringlength])
	 stringlength++;
	USBUartSendArray((uint8_t *)TerminalInfo, stringlength);
}

#define MAX_ARGLENGTH 10
void TerminalHandler(uint8_t input_char)
{
	static enum terminalstate{FIRST_CHAR, SECOND_CHAR, SPACE, ARGUMENT}TS_s;
	static uint8_t command[2];
	static uint8_t arg[MAX_ARGLENGTH];
	static uint8_t arglength;
	//display character if not line break (local echo)
	//if(! ((input_char == 0x0D) || (input_char == 0x0A)) )
	//	USBUartSendByte(input_char);
	switch(TS_s)
	{
		case FIRST_CHAR:
		{
			arglength = 0;
			if((input_char == 0x0D) || (input_char == 0x0A))
			{
				USBUartSendArray(prompt, 4);
			}
			else
			{
				command[0] = input_char;
				TS_s = SECOND_CHAR;
			}
			break;
		}
		case SECOND_CHAR:
		{
			if((input_char == 0x0D) || (input_char == 0x0A))
			{
				TS_s = FIRST_CHAR;
				USBUartSendArray(prompt, 4);
			}
			else
			{
				command[1] = input_char;
				TS_s = SPACE;
			}
			break;
		}
		case SPACE:
		{
			if(input_char != ' ')
			{
				if((input_char == 0x0D) || (input_char == 0x0A))
				{
					// Handle command without arguments, if no command found, return prompt
					if(TerminalCommandsNoArgument(command));
						USBUartSendArray(prompt, 4);
				}
				else
					USBUartSendArray(prompt, 4);
				TS_s = FIRST_CHAR;
			}
			else
			{
				TS_s = ARGUMENT;
			}
			break;
		}
		case ARGUMENT:
		{
			// if carriage return or linefeed
			if((input_char == 0x0D) || (input_char == 0x0A))
			{
				TS_s = FIRST_CHAR;
				if(TerminalCommandsWithArgument(command,arglength,arg))
					USBUartSendArray(prompt, 4);
			}
			else
			{
				if(arglength < MAX_ARGLENGTH)
				{
					arg[arglength] = input_char;
					arglength++;
				}
			}
			break;
		}
		default:
		{
			TS_s = FIRST_CHAR;
			USBUartSendArray(prompt, 4);
		}
	}
}

// handles commands with arguments
// returns zero if command was found and handled correctly

uint8_t TerminalCommandsWithArgument(uint8_t * command, uint8_t arglength, uint8_t * arg)
{
	static uint8_t response[20];
	switch(command[0])
	{
		case 'd':
		{
			uint32_t value;
			response[0]  = ';';
			response[1]  = ' ';
			response[2]  = ' ';
			response[3]  = ' ';
			response[4]  = ' ';
			response[5]  = ' ';
			response[6]  = ' ';
			response[7]  = ' ';
			response[8]  = ' ';
			response[9]  = prompt[0];
			response[10] = prompt[1];
			response[11] = prompt[2];
			response[12] = prompt[3];
			switch (command[1])
			{
				case '0': //d0
				{
					arg[arglength] = ' ';//take care that strtoul ends at end of input
					value = strtoul(arg, NULL,16);
					if(value >= UINT12_MAX)
						return 1;
					itoa((uint16_t)value, &(response[1]), 16);
					USBUartSendArray(response,13);
					DAC_2636WriteSingleChannel((uint16_t)value,DAC_2636_DAC_A);
					DAC_OFFSETS[0] = (uint16_t)value;
					break;
				}
				case '1': //d1
				{
					arg[arglength] = ' ';//take care that strtoul ends at end of input
					value = strtoul(arg, NULL,16);
					if(value >= UINT12_MAX)
						return 1;
					itoa((uint16_t)value, &(response[1]), 16);
					USBUartSendArray(response,13);
					DAC_2636WriteSingleChannel((uint16_t)value,DAC_2636_DAC_B);
					DAC_OFFSETS[1] = (uint16_t)value;
					break;
				}
				case '2': //d2
				{
					arg[arglength] = ' ';//take care that strtoul ends at end of input
					value = strtoul(arg, NULL,16);
					if(value >= UINT12_MAX)
						return 1;
					itoa((uint16_t)value, &(response[1]), 16);
					USBUartSendArray(response,13);
					DAC_2636WriteSingleChannel((uint16_t)value,DAC_2636_DAC_C);
					DAC_OFFSETS[2] = (uint16_t)value;
					break;
				}
				case '3': //d3
				{
					arg[arglength] = ' ';//take care that strtoul ends at end of input
					value = strtoul(arg, NULL,16);
					if(value >= UINT12_MAX)
						return 1;
					itoa((uint16_t)value, &(response[1]), 16);
					USBUartSendArray(response,13);
					DAC_2636WriteSingleChannel((uint16_t)value,DAC_2636_DAC_D);
					DAC_OFFSETS[3] = (uint16_t)value;
					break;
				}
				case '4': //d4
				{
					arg[arglength] = ' ';//take care that strtoul ends at end of input
					value = strtoul(arg, NULL,16);
					if(value >= UINT12_MAX)
						return 1;
					itoa((uint16_t)value, &(response[1]), 16);
					USBUartSendArray(response,13);
					DAC_2636WriteSingleChannel((uint16_t)value,DAC_2636_DAC_E);
					DAC_OFFSETS[4] = (uint16_t)value;
					break;
				}
				case '5': //d5
				{
					arg[arglength] = ' ';//take care that strtoul ends at end of input
					value = strtoul(arg, NULL,16);
					if(value >= UINT12_MAX)
						return 1;
					itoa((uint16_t)value, &(response[1]), 16);
					USBUartSendArray(response,13);
					DAC_2636WriteSingleChannel((uint16_t)value,DAC_2636_DAC_F);
					DAC_OFFSETS[5] = (uint16_t)value;
					break;
				}
				default:
				{
					return 1;
				}
			}
			break;
		} //end of case 'd'
		default:
			return 1;
	}
	return 0;
}

// handles commands with no arguments
// returns zero if command was found
uint8_t TerminalCommandsNoArgument(uint8_t * command)
{
	static uint8_t response[35]; //static in order to be able to pass to Uart function
	switch(command[0])
	{
		case '?':
		{
			if(command[1] == '?')
				TerminalShowInfo();
			else
				return 1;
			break;
		}
		case 'c':
		{
			if (command[1] == 't')
			{
				USBUartContinuousData = 1;
			}
			else if(command[1] == 'f')
			{
				USBUartContinuousData = 0;
			}
			break;
		}
		case 'd':
		{
			response[0]  = ';';
			response[1]  = '0';
			response[2]  = 'x';
			response[3]  = ' ';
			response[4]  = ' ';
			response[5]  = ' ';
			response[6]  = ' ';
			response[7]  = ' ';
			response[8]  = ' ';
			response[9]  = prompt[0];
			response[10] = prompt[1];
			response[11] = prompt[2];
			response[12] = prompt[3];
			switch(command[1])
			{
				case '0': //d0
				{
					itoa(DAC_OFFSETS[0],&(response[3]),16);
					USBUartSendArray(response,13);
					break;
				}
				case '1': //d1
				{
					itoa(DAC_OFFSETS[1],&(response[3]),16);
					USBUartSendArray(response,13);
					break;
				}
				case '2': //d2
				{
					itoa(DAC_OFFSETS[2],&(response[3]),16);
					USBUartSendArray(response,13);
					break;
				}
				case '3': //d3
				{
					itoa(DAC_OFFSETS[3],&(response[3]),16);
					USBUartSendArray(response,13);
					break;
				}
				case '4': //d4
				{
					itoa(DAC_OFFSETS[4],&(response[3]),16);
					USBUartSendArray(response,13);
					break;
				}
				case '5': //d5
				{
					itoa(DAC_OFFSETS[5],&(response[3]),16);
					USBUartSendArray(response,13);
					break;
				}
				default:
					return 1;
			}
			break;
		}// end of case 'd'
		case 'a':
		{
			response[0]  = ';';
			response[1]  = '0';
			response[2]  = 'x';
			response[3]  = ' ';
			response[4]  = ' ';
			response[5]  = ' ';
			response[6]  = ' ';
			response[7]  = ' ';
			response[8]  = ' ';
			response[9]  = prompt[0];
			response[10] = prompt[1];
			response[11] = prompt[2];
			response[12] = prompt[3];
			switch (command[1])
			{
				case '0': //a0
				{
					itoa(adc_results[0], &(response[3]), 16);
					USBUartSendArray(response,13);
					break;
				}
				case '1': //a1
				{
					itoa(adc_results[1], &(response[3]), 16);
					USBUartSendArray(response,13);
					break;
				}
				case '2': //a2
				{
					itoa(adc_results[2], &(response[3]), 16);
					USBUartSendArray(response,13);
					break;
				}
				case '3': //a3
				{
					itoa(adc_results[3], &(response[3]), 16);
					USBUartSendArray(response,13);
					break;
				}
				case '4': //a4
				{
					itoa(adc_results[4], &(response[3]), 16);
					USBUartSendArray(response,13);
					break;
				}
				case '5': //a5
				{
					itoa(adc_results[5], &(response[3]), 16);
					USBUartSendArray(response,13);
					break;
				}
				case '*': //a*
				{
					if(!SPIBufferAvailable())
						break;
				    response[0] = '\n';
					itoa_adc(adc_results[0], &(response[1]), 16, 4);
					response[5] = ' ';
					itoa_adc(adc_results[1], &(response[6]), 16, 4);
					response[10] = ' ';
					itoa_adc(adc_results[2], &(response[11]), 16, 4);
					response[15] = ' ';
					itoa_adc(adc_results[3], &(response[16]), 16, 4);
					response[20] = ' ';
					itoa_adc(adc_results[4], &(response[21]), 16, 4);
					response[25] = ' ';
                    itoa_adc(adc_results[5], &(response[26]), 16, 4);
					response[30] = prompt[0];
					response[31] = prompt[1];
					response[32] = prompt[2];
					response[33] = prompt[3];
					USBUartSendArray(response,34);
					break;
				}
				default:
					return 1;
			}
			break;
		}// end of case 'a'
		default:
			return 1;
	}
	return 0;
}

void itoa_adc(int16_t value, uint8_t * outbuffer, uint8_t radix, uint8_t length )
{
	uint8_t tempresult [30];
	volatile uint8_t resultlengthcounter;
	volatile int8_t outbuffercounter;
	volatile int16_t test;
	test = value;
	//clear result buffer
	for (outbuffercounter = 0 ; outbuffercounter < length ; outbuffercounter++ )
	{
		outbuffer[outbuffercounter] = '0';
	}
	itoa(-value, tempresult, 16);
	for (resultlengthcounter = 0; resultlengthcounter < 29 ; resultlengthcounter++ )
	{
		if (tempresult[resultlengthcounter] == 0)
		{
			//counter gives length of string (including NULL)!
			break;
		}
	}
	// if length of result within boundaries
	if ((resultlengthcounter <= length) && (resultlengthcounter > 0))
	{
		uint8_t difflength;
		difflength = length - resultlengthcounter;
		//decrease length to skip index to NULL
		for(outbuffercounter = 0 ; outbuffercounter < resultlengthcounter; outbuffercounter++)
		{
			outbuffer[outbuffercounter + difflength] = tempresult[outbuffercounter];
		}
	}
}

void ConvertADCResults(uint8_t *inbuffer, int16_t *outbuffer)
{
    uint8_t index;
    volatile int16_t temp;
    for(index = 0 ; index < 6 ; index++)
    {
        //add bytes from inbuffer, and mask / complete leading 2 bits)
        temp = ((int16_t)(inbuffer[index*2]) << 8 ) + inbuffer[(index*2)+1];
		if(temp & 0x2000)
			temp |= 0xC000;
		else
			temp &= 0x3FFF;
        outbuffer[index] = temp;
    }
}

void InitPorts(void)
{
    PORTA.DIR    = 0;
    PORTA.DIRSET = 0x1; //LED RED
    PORTA.DIRSET = 0x2; //LED GREEN
    PORTA.OUT    = 0x3; //BOTH LEDS OFF

    PORTB.DIR    = 0;
    PORTB.OUT    = 0;
//PORTA.DIR = PIN3_bm | PIN6_bm;  //sets pins 3 and 6 as outputs, therefore the other pins will be inputs
//PORTA.OUTSET = PIN1_bm;  // set A1 to 1
//PORTA.OUTCLR = PIN2_bm;  // set A2 to 0
    PORTC.DIR    = 0;
    PORTC.OUT    = 0x02; //nUSB-RESET
    PORTC.DIRSET = 0x02; //nUSB-RESET
    PORTC.DIRSET = 0x08; //TX USB
	//PORTC.PIN5CTRL |= PORT_OPC_PULLDOWN_gc;//input for xsens
    //set PC5 as output
	PORTC.DIRSET = PIN5_bm; //SyncOut   (instead of the xbus input)
	
	PORTD.DIR    = 0;
    PORTD.DIRSET = 0x01; //ADC_CONV_CLK
    PORTD.DIRSET = 0x02; //DAC_nLDAC
    PORTD.DIRSET = 0x04; //DAC_nCLR
    PORTD.DIRSET = 0x08; //CONV
    PORTD.DIRSET = 0x10; //nCS-DAC
    PORTD.DIRSET = 0x20; //SPI_MOSI
    PORTD.DIRSET = 0x80; //SPI_MISO
    PORTD.OUT    = 0b00010001;
	PORTD.PIN3CTRL  |= PORT_SRLEN_bm;

    PORTE.DIR    = 0;
    PORTE.DIRSET = 0x01;  //ADC-SEL0
	PORTE.DIRSET = 0x02;  //ADC-SEL1
	PORTE.DIRSET = 0x04;  //ADC-SEL2
    PORTE.DIRSET = 0x08;  //ADC_BIP
    PORTE.OUT    = 0;
}


void SetClock(void){
   //clock initialisation
   /* Start up external 16MHz , multiply by 2 with PLL*/
    OSC.XOSCCTRL = (OSC_FRQRANGE_12TO16_gc| OSC_XOSCSEL_XTAL_16KCLK_gc);  // configure to use external xtal, 16MHz
    OSC.CTRL    |= OSC_XOSCEN_bm;                                         // enable external oscillator
    loop_until_bit_is_set(OSC.STATUS, OSC_XOSCRDY_bp);                    // Wait until oscillator is ready
    OSC.PLLCTRL  = OSC_PLLSRC_XOSC_gc | 2;                                // CLK source for PLL is XOSC, multiplier = 2 
    OSC.CTRL    |= OSC_PLLEN_bm;                                          // Enable PLL
    loop_until_bit_is_set(OSC.STATUS, OSC_PLLRDY_bp);                     // Wait until PLL is ready
    CCP          = CCP_IOREG_gc;                                          // enable configuration change
    CLK.CTRL     = CLK_SCLKSEL_PLL_gc;                                    // selecteer PLL als clock bron
    OSC.CTRL    &= ~OSC_RC2MRDY_bp;                                       // disable 2MHz RC oscillator
}

void SetTimer(void)
{
    //OC0A on PORTE is connected to SYNC-IMU
    TCC0.CTRLA    = TC_CLKSEL_DIV64_gc;
    TCC0.CTRLB    = TC0_CCAEN_bm /*| TC0_CCBEN_bm*/ | TC_WGMODE_FRQ_gc;
    TCC0.CTRLC    = 0;
    TCC0.CTRLD    = TC_EVACT_OFF_gc | TC_EVSEL_OFF_gc;     // no events
    TCC0.CTRLE    = 0;                                     // no byte mode
    //TCC0.PER      = (4 * US_PWM_PERIOD);       // 10ms at 32MHz, clk divided by 8
    TCC0.CCA      = (US_PERIOD);         // number of ms for PWM generation
    //TCC0.CCB    = (4 * US_PWM_READ_BMA);     // number of ms after which BMA should be ready, HMC can be read.
    TCC0.INTFLAGS = 0xF3;   // clear all interrupt flags by writing '1'
    TCC0.INTCTRLA = TC_ERRINTLVL_OFF_gc | TC_OVFINTLVL_OFF_gc;  // error interrupt disabled / overflow interrupt enabled
    TCC0.INTCTRLB = TC_CCDINTLVL_OFF_gc | TC_CCCINTLVL_OFF_gc | TC_CCBINTLVL_OFF_gc | TC_CCAINTLVL_HI_gc;
}


/*number of bytes is only used in CLONE mode*/
void CopyArray(uint8_t * p_source, uint8_t * p_dest, uint8_t number_of_bytes, uint8_t mode)
{
    uint8_t counter;
    switch(mode)
    {
        case CLONE:
        {
            for(counter = 0 ; counter < number_of_bytes ; counter++)
            {
                *(p_dest + counter) = *(p_source + counter);
            }
            break;
        }
        case COPY_BMA:
        {
            /*MSB X*/
            p_dest[0]   =  p_source[1] >> 2;
            if(p_source[1] & 0x80)            // if negative
                p_dest[0] |= 0xC0;              // add ones
            /*LSB X*/
            p_dest[1] =  ( p_source[1] <<6 ) | ( p_source[0] >>2);
            /*MSB Y*/
            p_dest[2]   =  p_source[3] >> 2;
            if(p_source[3] & 0x80)            // if negative
                p_dest[2] |= 0xC0;              // add ones
            /*LSB Y*/
            p_dest[3] =  ( p_source[3] <<6 ) | ( p_source[2] >>2);
            /*MSB Z*/
            p_dest[4]   =  p_source[5] >> 2;
            if(p_source[5] & 0x80)            // if negative
                p_dest[4] |= 0xC0;              // add ones
            /*LSB Z*/
            p_dest[5] =  ( p_source[5] <<6 ) | ( p_source[4] >>2);
            /*TEMP*/
            p_dest[6] = p_source[6];
            break;
        }
        case COPY_HMC:
        {
            /*MSB X*/
            p_dest[0] = p_source[0];
            /*LSB X*/
            p_dest[1] = p_source[1];
            /*MSB Y*/
            p_dest[2] = p_source[4];
            /*LSB Y*/
            p_dest[3] = p_source[5];
            /*MSB Z*/
            p_dest[4] = p_source[2];
            /*LSB Z*/
            p_dest[5] = p_source[3];
            break;
        }
        default:
        {
            break;
        }
    }
}
