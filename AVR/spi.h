#ifndef _SPI_H_
    #define _SPI_H_ 1

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define true  1
#define false 0

//General SPI
#define SPI_MOSI            0x20
#define SPI_MISO            0x40
#define SPI_SCK             0x80
#define SPI_ADC_DAC_vect    SPID_INT_vect
#define SPI_ADC_DAC         (&SPID)
//ADC spi
#define ADC_1408_SPI        (&SPID)
#define ADC_1408_SPI_PORT   (&PORTD)
#define ADC_1408_CONV       0x08
#define ADC_1408_CONV_CLK   0x01
// ADC SPI MACROS
#define ADC_1408_CONV_HIGH      ADC_1408_SPI_PORT->OUTSET = ADC_1408_CONV
#define ADC_1408_CONV_LOW       ADC_1408_SPI_PORT->OUTCLR = ADC_1408_CONV
#define ADC_1408_CONVCLOCK_HIGH ADC_1408_SPI_PORT->OUTSET = ADC_1408_CONV_CLK
#define ADC_1408_CONVCLOCK_LOW  ADC_1408_SPI_PORT->OUTCLR = ADC_1408_CONV_CLK
//ADC configuration
#define ADC_1408_CONF_PORT  (&PORTE)
#define ADC_1408_SEL0       0x01
#define ADC_1408_SEL1       0x02
#define ADC_1408_SEL2       0x04
#define ADC_1408_BIP        0x08
//DAC spi
#define DAC_2636_SPI        (&SPID)
#define DAC_2636_SPI_PORT   (&PORTD)
#define DAC_2636_N_CS       0x10
//DAC configuration
#define DAC_2636_CONF_PORT  (&PORTD)
#define DAC_2636_N_LDAC     0x02
#define DAC_2636_N_CLR      0x04

#define LTC2636_CMD_ADDR(x,y) (x<<4)|y;
//LTC2636 COMMAND
#define DAC_2636_WRITE_INPUT_N              0b0000
#define DAC_2636_UPDATE_DAC_N               0b0001
#define DAC_2636_WRITE_INPUT_N_UPDATE_ALL   0b0010
#define DAC_2636_WRITE_INPUT_AND_UPDATE_N   0b0011
#define DAC_2636_POWER_DOWN_N               0b0100
#define DAC_2636_POWER_DOWN_CHIP            0b0101
#define DAC_2636_SELECT_INTERNAL_REF        0b0110
#define DAC_2636_SELECT_EXTERNAL_REF        0b0111
#define DAC_2636_NOP                        0b1111
//LTC2636 ADDRESS
#define DAC_2636_DAC_A   0b0000
#define DAC_2636_DAC_B   0b0001
#define DAC_2636_DAC_C   0b0010
#define DAC_2636_DAC_D   0b0011
#define DAC_2636_DAC_E   0b0100
#define DAC_2636_DAC_F   0b0101
#define DAC_2636_DAC_G   0b0110
#define DAC_2636_DAC_H   0b0111
#define DAC_2636_DAC_ALL 0b1111

//LTC1408
// One lower than amount of inputs to be sampled
#define ADC_1408_NR_INPUTS     5
//Function Definitions
void DAC_2636_init(void);
void ADC_1408_init(void);
void InitADCClock(void);
//SPI functions
inline void SPI_DAC_2636_init(void);
inline void SPI_ADC_1408_init(void);
uint8_t SPITransceiveArray(uint8_t * , uint8_t, uint8_t );
inline void SPI_SendByte(SPI_t *, uint8_t);
inline uint8_t SPI_RecvByte(SPI_t *);
uint8_t DAC_2636WriteSingleChannel(uint16_t, uint8_t);
void SPIClearBuffer(void);
uint8_t SPITransferReady(void);
enum spitargets SPITarget(void);
uint8_t * SPIbufferpointer(void);
uint8_t   SPIBufferAvailable(void);
uint8_t SPIDataHandled(void);
void SPISetDataHandled(uint8_t);
//enums
enum spitargets{ADC = 0, DAC};
enum SPI_RETURN_VALUES{SPI_OK = 0, SPI_ERROR, SPI_ERROR_BUFFER};
#endif //_SPI_H
