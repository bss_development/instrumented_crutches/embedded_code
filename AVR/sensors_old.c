#include "sensors.h"
#include <util/delay.h>

uint8_t SensorHMCStartSingle[] = {6,I2C_START,HMC_WRITE,0x02,HMC_MR, MD_SING,I2C_STOP};
uint8_t SensorHMCGetData[] = {4,I2C_START,HMC_READ,6,I2C_STOP};
uint8_t SensorBMAGetData[] = {8,I2C_START,BMA_WRITE,0x01,BMA_ACC_X_LSB,I2C_START,BMA_READ,7,I2C_STOP};

void SensorSetupBMAPinInterrupts(void)
{
    //ACC1
    SENS1_PORT.SENS1_INTMASK = SENS1_ACC_DRDY_BM;
    PORTCFG.MPCMASK          = SENS1_ACC_DRDY_BM;  // Determine which PINnCTRL register to write to
    SENS1_PORT.PIN1CTRL      = PORT_ISC_RISING_gc; // Target register determined by MPCMASK in previous statement
    //ACC2
    SENS2_PORT.SENS2_INTMASK = SENS2_ACC_DRDY_BM;
    PORTCFG.MPCMASK          = SENS2_ACC_DRDY_BM;
    SENS2_PORT.PIN1CTRL      = PORT_ISC_RISING_gc;
    //ACC3
    SENS3_PORT.SENS3_INTMASK = SENS3_ACC_DRDY_BM;
    PORTCFG.MPCMASK          = SENS3_ACC_DRDY_BM;
    SENS3_PORT.PIN1CTRL      = PORT_ISC_RISING_gc;
    //ACC4
    SENS4_PORT.SENS4_INTMASK = SENS4_ACC_DRDY_BM;
    PORTCFG.MPCMASK          = SENS4_ACC_DRDY_BM;
    SENS4_PORT.PIN1CTRL      = PORT_ISC_RISING_gc;
    //ACC5
    SENS5_PORT.SENS5_INTMASK = SENS5_ACC_DRDY_BM;
    PORTCFG.MPCMASK          = SENS5_ACC_DRDY_BM;
    SENS5_PORT.PIN1CTRL      = PORT_ISC_RISING_gc;
}

void SensorSetupBMA180(I2CPORT * port)
{
    uint8_t bw_tcs_val;
    uint8_t ctrl_reg3_val;
    uint8_t offset_lsb1_val;
	uint8_t tco_z_val;
	uint8_t temp;
    while(port->tx.state != I2C_IDLE);
	while(!I2CcheckIfBusFree(port,10));                             //wait until bus is free
    offset_lsb1_val  = *(I2CReadBytes(port, BMA_READ, BMA_OFFSET_LSB1, 1) ); // lees waarde terug om callibratiewaarden te houden
    offset_lsb1_val &= ~(RANGE_MASK);
    offset_lsb1_val |=  (RANGE_8G);                                // 16G range

    SensorWriteRegValue(port, BMA_WRITE, BMA_CTRL_REG0, BMA_EE_W ); //Enable writing of image data
    while(!I2CcheckIfBusFree(port,10));
	SensorWriteRegValue(port, BMA_WRITE, BMA_OFFSET_LSB1, offset_lsb1_val);
	while(!I2CcheckIfBusFree(port,10));

    bw_tcs_val   = *(I2CReadBytes(port, BMA_READ, BMA_BW_TCS, 1) ); // lees waarde terug om callibratiewaarden te houden
    bw_tcs_val  &= ~(BW_MASK);
    bw_tcs_val  |= (BW_300);                                       // 1200Hz bandwidth

	SensorWriteRegValue(port, BMA_WRITE, BMA_BW_TCS, bw_tcs_val);
    while(!I2CcheckIfBusFree(port,10));                             //wait until bus is free

	tco_z_val  = *(I2CReadBytes(port, BMA_READ, BMA_TCO_Z, 1) );
    tco_z_val &= ~(MODE_CONFIG_MASK);
    tco_z_val |=  (MODE_ULN);
    SensorWriteRegValue(port, BMA_WRITE, BMA_TCO_Z, tco_z_val);     //Write power / noise config
    while(!I2CcheckIfBusFree(port,10));

    SensorWriteRegValue(port, BMA_WRITE, BMA_CTRL_REG0, 0x00 );     //Disable writing of image data
    while(!I2CcheckIfBusFree(port,10));

	I2CReadBytes(port, BMA_READ, BMA_ACC_X_LSB, 7);
	while(!I2CcheckIfBusFree(port,10));                             //wait until bus is free
    I2CReadBytes(port, BMA_READ, BMA_OFFSET_LSB1, 1);
}

void SensorSetupHMC(I2CPORT * port)
{
    while(port->tx.state != I2C_IDLE);   // wait until transmitter ready
    port->tx.buffer[0] = 8;
    port->tx.buffer[1] = I2C_START;
    port->tx.buffer[2] = HMC_WRITE;
    port->tx.buffer[3] = 4;             // stuur 4 data bytes (adres + 3 waarden)
    port->tx.buffer[4] = HMC_CRA;       // start op adres 0
    port->tx.buffer[5] = AVG_1 | DOR_75HZ | MM_NORMAL;  // CRA : average 1, Data output rate 75Hz, normal mode (no bias)
    port->tx.buffer[6] = GN_0_88GA;     // +/-0.88Gauss Full Scale Range
    port->tx.buffer[7] = MD_SING;       // Single Measurement Mode
    port->tx.buffer[8] = I2C_STOP;
	while(!I2CcheckIfBusFree(port,5));               //wait until bus is free
    I2CSendMessage(port, port->tx.buffer);
	while(!I2CcheckIfBusFree(port,5));               //wait until bus is free
	I2CWriteIOPort(port, (LED_RED_OFF_bm));
}

inline void SensorWriteRegValue(I2CPORT * port, uint8_t adr, uint8_t reg, uint8_t val)
{
    while(!port->tx.message_complete); // wait until transmitter ready
    port->tx.buffer[0] = 6;            // transmit 5 bytes
    port->tx.buffer[1] = I2C_START;
    port->tx.buffer[2] = adr;
    port->tx.buffer[3] = 2;            // schrijf 2 bytes
    port->tx.buffer[4] = reg;          // register
    port->tx.buffer[5] = val;          // value
    port->tx.buffer[6] = I2C_STOP;
    I2CSendMessage(port, port->tx.buffer);
}

// If mask is changed, also change in ISR(SENSX_ACC_DRDY_vect)
void SensorEnableBMAPinInterrupts(void)
{
    SENS1_PORT.INTCTRL |= SENS1_INTLVL_HI
    SENS2_PORT.INTCTRL |= SENS2_INTLVL_HI;
    SENS3_PORT.INTCTRL |= SENS2_INTLVL_HI;
    SENS4_PORT.INTCTRL |= SENS2_INTLVL_HI;
    SENS5_PORT.INTCTRL |= SENS2_INTLVL_HI;
}
