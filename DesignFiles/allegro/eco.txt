|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   1  |
|------------------------------------------------------------------------------|
|  d:.../xbusforcesensor/active/designfiles/allegro/pcbxbusforce.brd           |
|                                                    Wed Aug 17 14:49:46 2011  |
|------------------------------------------------------------------------------|
| NET CHANGES                                                                  |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|   net  name |  type of change |    pin_id     |   x   |   y   |   to  net    |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
  N59758_SHEET ? pins ADDED TO this new net
                                 R76.1                          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N59774_SHEET ? pins ADDED TO this new net
                                 R77.1                          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  VCC          pins MOVED FROM this net (to net name listed on right)
                                 U14.8           19.3500  5.2950
                                                                          VP3V3
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  VCC_BAR      pins MOVED FROM this net (to net name listed on right)
                                 C16.1           12.1500 38.0000
                                                                          VP3V3
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  XBUS_DATA_A  pins MOVED FROM this net (to net name listed on right)
                                 U14.6           19.3500  2.7550
                                                                 N59774_SHEET ?
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
               pins ADDED TO this existing net (pins not previously on any net)
                                 R77.2                          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  XBUS_DATA_B  pins MOVED FROM this net (to net name listed on right)
                                 U14.7           19.3500  4.0250
                                                                 N59758_SHEET ?
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
               pins ADDED TO this existing net (pins not previously on any net)
                                 R76.2                          
|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   2  |
|------------------------------------------------------------------------------|
|  d:.../xbusforcesensor/active/designfiles/allegro/pcbxbusforce.brd           |
|                                                    Wed Aug 17 14:49:46 2011  |
|------------------------------------------------------------------------------|
| COMPONENT DEFINITION added to board drawing                                  |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|    device type                                                               |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
 DS485-POWER_SOIC127P600X175-8N_
 RESISTOR_RESC1608X84N_22E
|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   3  |
|------------------------------------------------------------------------------|
|  d:.../xbusforcesensor/active/designfiles/allegro/pcbxbusforce.brd           |
|                                                    Wed Aug 17 14:49:46 2011  |
|------------------------------------------------------------------------------|
| COMPONENTS ADDED to board drawing                                            |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|    ref des        |    device type                                           |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
 R76                 RESISTOR_RESC1608X84N_22E 
 R77                 RESISTOR_RESC1608X84N_22E 
|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   4  |
|------------------------------------------------------------------------------|
|  d:.../xbusforcesensor/active/designfiles/allegro/pcbxbusforce.brd           |
|                                                    Wed Aug 17 14:49:46 2011  |
|------------------------------------------------------------------------------|
| COMPONENTS CHANGED from one device type to another in board drawing          |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|    ref des        | new device type     |   x   |   y   |  old  device type  |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
 U14                 DS485-POWER_SOIC127P600X175-8N_ 16.6500  3.3900 DS485_SOIC127P600X175-8N_DS485
|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   5  |
|------------------------------------------------------------------------------|
|  d:.../xbusforcesensor/active/designfiles/allegro/pcbxbusforce.brd           |
|                                                    Wed Aug 17 14:49:46 2011  |
|------------------------------------------------------------------------------|
| SLOT PROPERTIES added to board drawing                                       |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|   slot_id    |   x   |   y   |   property   |             value              |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
 R76.2                          PRIM_FILE      .\pstchip.dat
 R76.2                          SECTION        
 R77.2                          PRIM_FILE      .\pstchip.dat
 R77.2                          SECTION        
 U14.8          16.6500  3.3900 PRIM_FILE      .\pstchip.dat
 U14.8          16.6500  3.3900 SECTION        
|------------------------------------------------------------------------------|
|   total ECO changes reported  19                                             |
|------------------------------------------------------------------------------|
